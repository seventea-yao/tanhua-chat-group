package com.itheima.appserver.service;

import com.itheima.commons.vo.CommentVo;
import com.itheima.commons.vo.PageResult;
import com.itheima.commons.vo.VideoVo;
import org.springframework.web.multipart.MultipartFile;

public interface VideoService {
    //上传视频
    void loadVideo(MultipartFile videoThumbnail, MultipartFile videoFile);

    //小视频列表
    PageResult videoList(Integer page, Integer pagesize);
    // 视频点赞
    void videoLike(String id);
    // 取消视频点赞
    void videoDisLike(String id);
    // 评论列表
    PageResult queryCommentsList(String movementId, Integer page, Integer pagesize);
    //视频评论点赞
    void videoCommentLike(String id);
    //取消评论点赞
    void videoCommentDisLike(String id);
    //关注视频用户
    void userFocus(String id);
    //取消视频用户关注
    void userUnFocus(String id);
    //视频评论
    void publishComment(String id, String context);
}
