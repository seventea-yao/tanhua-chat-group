package com.itheima.commons.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;

import java.io.Serializable;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TimeLine implements Serializable {
    private ObjectId id;
    private Long  userId;
    private ObjectId publishId;
    private Long date;
}
