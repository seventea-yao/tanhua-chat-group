package com.itheima.mysql.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.itheima.commons.pojo.Question;
import com.itheima.interfaces.QuestionApi;
import com.itheima.mysql.mapper.QuestionMapper;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class QuestionMapperImpl implements QuestionApi {

    @Autowired
    private QuestionMapper questionMapper;

    @Override
    public Question strangerQuestions(Long id) {
        Question question = questionMapper.selectOne(Wrappers.lambdaQuery(Question.class).eq(Question::getUserId, id));
        return question;
    }

    @Override
    public void saveQuestion(Question question) {
        questionMapper.insert(question);
    }

    @Override
    public void updateQuestion(Long userId, String content) {
        questionMapper.update(Question.builder().txt(content).build(), Wrappers.lambdaQuery(Question.class).eq(Question::getUserId, userId));
    }
}
