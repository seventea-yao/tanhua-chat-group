package com.itheima.mysql.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.commons.pojo.ManageUser;

public interface ManageUserMapper extends BaseMapper<ManageUser> {
}
