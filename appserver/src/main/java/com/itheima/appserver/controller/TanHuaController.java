package com.itheima.appserver.controller;

import com.itheima.appserver.service.TanHuaService;
import com.itheima.commons.vo.NearUserVo;
import com.itheima.commons.vo.PageResult;
import com.itheima.commons.vo.TodayBest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("tanhua")
public class TanHuaController {

    @Autowired
    private TanHuaService tanHuaService;

    /**
     * 今日佳人
     *
     * @return
     */
    @GetMapping("todayBest")
    public ResponseEntity todayBest() {
        TodayBest todayBest = tanHuaService.todayBest();
        return ResponseEntity.ok(todayBest);
    }

    /**
     * 推荐朋友
     *
     * @return
     */
    @GetMapping("recommendation")
    public ResponseEntity recommendation(@RequestParam Map<String, Object> map) {
        PageResult pageResult = tanHuaService.recommendation(map);
        return ResponseEntity.ok(pageResult);
    }

    /**
     * 查询主页详情信息
     *
     * @param id
     * @return
     */
    @GetMapping("{id}/personalInfo")
    public ResponseEntity personalInfo(@PathVariable("id") Long id) {
        TodayBest todayBest = tanHuaService.personalInfo(id);
        return ResponseEntity.ok(todayBest);
    }

    /**
     * 查询陌生人问题
     *
     * @param id
     * @return
     */
    @GetMapping("strangerQuestions")
    public ResponseEntity strangerQuestions(@RequestParam("userId") Long id) {
        String question = tanHuaService.strangerQuestions(id);
        return ResponseEntity.ok(question);
    }

    /**
     * 回答陌生人问题
     *
     * @param map
     * @return
     */
    @PostMapping("strangerQuestions")
    public ResponseEntity strangerQuestions(@RequestBody Map<String, Object> map) {
        tanHuaService.answerQuestion(map.get("userId"), map.get("reply"));
        return ResponseEntity.ok(null);
    }

    /**
     * 搜索附近
     *
     * @param
     * @return
     */
    @GetMapping("search")
    public ResponseEntity search(@RequestParam(value = "gender", required = false) String gender,
                                 @RequestParam(value = "distance", defaultValue = "2000") Double distance) {
        List<NearUserVo> search = tanHuaService.search(gender, distance);
        return ResponseEntity.ok(search);
    }

    /**
     * 随机卡片
     *
     * @return
     */
    @GetMapping("cards")
    public ResponseEntity cards() {
        List<TodayBest> todayBestList = tanHuaService.selectCards();
        return ResponseEntity.ok(todayBestList);
    }

    /**
     * 喜欢
     *
     * @return
     */
    @GetMapping("{id}/love")
    public ResponseEntity loveCards(@PathVariable("id") Long id) {
        tanHuaService.loveCard(id);
        return ResponseEntity.ok(null);
    }

    /**
     * 不喜欢
     *
     * @return
     */
    @GetMapping("{id}/unlove")
    public ResponseEntity unlove(@PathVariable("id") Long id) {
        tanHuaService.unloveCard(id);
        return ResponseEntity.ok(null);
    }

}
