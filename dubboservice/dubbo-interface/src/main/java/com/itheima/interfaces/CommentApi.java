package com.itheima.interfaces;

import com.itheima.commons.pojo.Comment;
import org.bson.types.ObjectId;

import java.util.List;

public interface CommentApi {


    /**
     * 根据发布表id 查询 评论表点赞数量
     * @param id
     * @return
     */
    Long selectLikeSize(String id);

    /**
     * 根据发布表id 查询 评论表评论数量
     * @param id
     * @return
     */
    Long selectCommentSize(String id);

    /**
     * 根据发布表id 查询 评论表喜欢数量
     * @param id
     * @return
     */
    Long selectLoveSize(String id);

    /**
     * 根据发布表id 和 用户id 查询评论表是否有点赞
     * @param id
     * @param userId
     * @return
     */
    boolean selectIsLike(String id, Long userId);

    /**
     * 根据发布表id 和 用户id 查询评论表是否有喜欢
     * @param id
     * @param userId
     * @return
     */
    boolean selectIsLove(String id, Long userId);

    /**
     * 发表评论
     * @param comment
     */
    void saveComment(Comment comment);

    Comment selectPublishUserId(ObjectId movementId);

    Comment selectById(String publishId);

    /**
     * 根据publish查询评论表
     * @param publishId
     * @param page
     * @param pagesize
     * @return
     */
    List<Comment> selectByPublishId(String publishId, Integer page, Integer pagesize);

    /**
     * 查询是否关注视频用户
     * @param userId
     * @param userId1
     * @return
     */
    Boolean selectIsFollow(Long userId, Long userId1);

    /**
     * 通过自己的id 找到评论过我的人 点赞/评论/喜欢
     * @param page
     * @param pagesize
     * @param userId
     * @param i
     * @return
     */
    List<Comment> selectByPublishUserId(Integer page, Integer pagesize, Long userId, int i);

    List<Comment> pageCommentsLogs(Integer page, Integer pageSize, String sortProp, String sortOrder, ObjectId messageID);
}
