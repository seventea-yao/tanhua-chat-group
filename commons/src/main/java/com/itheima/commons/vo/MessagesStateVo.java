package com.itheima.commons.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
//消息的审核状态数量
public class MessagesStateVo implements Serializable {

    private String title;//状态标题 	枚举: 全部,待审核,已通过,已驳回
    private String code;//状态码
    private Long value;//数量
}
