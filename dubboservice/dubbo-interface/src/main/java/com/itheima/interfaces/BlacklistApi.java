package com.itheima.interfaces;

import com.itheima.commons.pojo.BlackList;

import java.util.List;

public interface BlacklistApi {

    /**
     * 查询黑名单
     * @param userId
     * @param page
     * @param pagesize
     * @return
     */
    List<BlackList> selectBlackList(Long userId, Integer page, Integer pagesize);

    /**
     * 根据自己id 和用户id 移除黑名单
     * @param userId
     * @param uid
     */
    void deleteBlackList(Long userId, Long uid);
}
