package com.itheima.interfaces;

import com.itheima.commons.pojo.Album;

import java.util.List;

public interface AlbumApi {
    void savePublishId(Album album,Long userId);

    /**
     * 根据自己的id 查询自己的全部动态
     * @param page
     * @param pagesize
     * @param userId
     * @return
     */
    List<Album> selectByUserId(Integer page, Integer pagesize, Long userId);
    /**
     * 根据自己的id 查询自己的全部动态 降序查
     * @param page
     * @param pagesize
     * @param userId
     * @return
     */
    List<Album> selectByUserIdAndDesc(Integer page, Integer pagesize, Long userId);

    /**
     * 根据userId 查询全部动态数量
     * @param userId
     * @return
     */
    Long findCountByUserId(Long userId);
}
