package com.itheima.appserver.service.impl;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.appserver.exception.MyException;
import com.itheima.appserver.exception.ResultError;
import com.itheima.appserver.interceptor.UserThreadLocal;
import com.itheima.appserver.service.UserInfoService;
import com.itheima.commons.pojo.UserInfo;
import com.itheima.commons.utils.FaceDetectionUtil;
import com.itheima.commons.utils.UploadPicUtil;
import com.itheima.interfaces.UserInfoApi;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;

@Service
public class UserInfoServiceImpl implements UserInfoService {

    @Reference
    private UserInfoApi userInfoAPI;

    @Override
    public void loginReginfo(Map<String, String> map) {
       /* // 解析密文
        JWT jwt = JWTUtil.parseToken(token);
        jwt.setKey(Constants.JWT_SECRET.getBytes());


        //校验密文是否失效
        if (!jwt.verify()) {
            throw new MyException(ResultError.tokenLoseError());
        }

        //校验密文是否过期
        if (!jwt.validate(0)) {
            throw new MyException(ResultError.tokenOutTimeError());

        }*/

        //校验用户数据是否完善
        if (StrUtil.hasEmpty(map.get("gender"),map.get("nickname"),map.get("birthday"),map.get("city"))) {
            throw new MyException(ResultError.UserInfoError());

        }

        //添加信息到数据库
        userInfoAPI.insertUserInfo(UserInfo
                .builder()
                .userId(Convert.toLong(UserThreadLocal.getUserId()))
                .nickName(map.get("nickname"))
                .birthday(map.get("birthday"))
                .city(map.get("city"))
                .sex(map.get("gender").equalsIgnoreCase("man")?1:2)
                .build());
    }

    @Override
    public void updateHead(MultipartFile multipartFile) {
    /*    // 解析密文
        JWT jwt = JWTUtil.parseToken(token);
        jwt.setKey(Constants.JWT_SECRET.getBytes());

        //校验密文是否失效
        if (!jwt.verify()) {
            throw new MyException(ResultError.tokenLoseError());

        }

        //校验密文是否过期

        if (!jwt.validate(0)) {
            throw new MyException(ResultError.tokenOutTimeError());

        }*/
        //文件格式是否不正确
        if (!StrUtil.endWithAnyIgnoreCase(multipartFile.getOriginalFilename(),".png","jpeg","jpg")) {
            throw new MyException(ResultError.fileFormatError());

        }
        //文件大小是否符合
        if ((multipartFile.getSize()/1024/1024)>=2){
            throw new MyException(ResultError.fileFormatError());

        }
        //判断是否为人脸
        try {
            Boolean faceDetection = FaceDetectionUtil.FaceDetection(multipartFile.getBytes());
            if (!faceDetection){
                throw new MyException(ResultError.faceError());
            }
            //把图片路径存到oss
            String file = UploadPicUtil.uploadPic(multipartFile.getOriginalFilename(), multipartFile.getBytes());
            //添加图片到数据库
            userInfoAPI.updateUserInfoByLogo(Convert.toLong(UserThreadLocal.getUserId()),file);
        } catch (IOException e) {
            throw new MyException(ResultError.faceUploadError());
        }


    }
}
