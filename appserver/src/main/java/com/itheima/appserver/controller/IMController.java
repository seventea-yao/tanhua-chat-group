package com.itheima.appserver.controller;

import com.itheima.appserver.interceptor.UserThreadLocal;
import com.itheima.appserver.service.IMService;
import com.itheima.commons.vo.PageResult;
import com.itheima.commons.vo.UserInfoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("messages")
public class IMController {

    @Autowired
   private IMService imService;

/**
     * 根据环信id 查询用户信息
     * @param huanxinId
     * @return
     */

    @GetMapping("userinfo")
    public ResponseEntity userInfo(@RequestParam("huanxinId") String huanxinId) {
        UserInfoVo userInfoVo =imService.selectUserInfo(huanxinId);
        return ResponseEntity.ok(userInfoVo);
    }

    /**
     * 添加好友
     * @param userId
     * @return
     */
    @PostMapping("contacts")
    public ResponseEntity contacts(@RequestBody Integer userId) {
        imService.contacts(userId);
        return ResponseEntity.ok(null);
    }

    /**
     * 查询联系人列表
     * @param page
     * @param pagesize
     * @param keyword
     * @return
     */
    @GetMapping("contacts")
    public ResponseEntity contacts(@RequestParam(value = "page",defaultValue = "1",required = false)Integer page,
                                   @RequestParam(value = "pagesize",defaultValue = "10",required = false)Integer pagesize,
                                   @RequestParam(value = "keyword",required = false)String keyword){
        PageResult pageRequest =imService.selectLinkman(page,pagesize,keyword);
        return ResponseEntity.ok(pageRequest);
    }

    /**
     * 点赞列表
     * @param page
     * @param pagesize
     * @return
     */
    @GetMapping("likes")
    public ResponseEntity likes(@RequestParam(value = "page" ,defaultValue = "1",required = false)Integer page,
                                @RequestParam(value = "pagesize",defaultValue = "10",required = false)Integer pagesize){
        PageResult pageResult =imService.selectListCount(page,pagesize, UserThreadLocal.getUserId(),1);
        return ResponseEntity.ok(pageResult);
    }

    /**
     * 评论列表
     * @param page
     * @param pagesize
     * @return
     */
    @GetMapping("comments")
    public ResponseEntity comments(@RequestParam(value = "page",defaultValue = "1",required = false)Integer page,
                                @RequestParam(value = "pagesize",defaultValue = "10",required = false)Integer pagesize){
        PageResult pageResult =imService.selectListCount(page,pagesize, UserThreadLocal.getUserId(),2);
        return ResponseEntity.ok(pageResult);
    }

    /**
     * 喜欢列表
     * @param page
     * @param pagesize
     * @return
     */
    @GetMapping("loves")
    public ResponseEntity loves(@RequestParam(value = "page",defaultValue = "1",required = false)Integer page,
                                        @RequestParam(value = "pagesize",defaultValue = "10",required = false)Integer pagesize){
        PageResult pageResult =imService.selectListCount(page,pagesize, UserThreadLocal.getUserId(),3);
        return ResponseEntity.ok(pageResult);
    }

    /**
     * 公告列表
     * @param page
     * @param pagesize
     * @return
     */
    @GetMapping("announcements")
    public ResponseEntity announcements(@RequestParam(value = "page",defaultValue = "1",required = false)Integer page,
                                @RequestParam(value = "pagesize",defaultValue = "10",required = false)Integer pagesize){
        PageResult pageResult =imService.selectAnnouncements(page,pagesize);
        return ResponseEntity.ok(pageResult);
    }
}
