package com.itheima.interfaces;

import com.itheima.commons.pojo.UserInfoData;

import java.util.List;

public interface UserInfoDataApi {
    List<UserInfoData> selectNowDayUser(String s, String s1, Integer sex);
}
