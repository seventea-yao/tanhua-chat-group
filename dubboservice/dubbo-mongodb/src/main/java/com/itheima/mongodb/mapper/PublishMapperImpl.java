package com.itheima.mongodb.mapper;

import cn.hutool.core.collection.CollUtil;
import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.commons.pojo.Comment;
import com.itheima.commons.pojo.Publish;
import com.itheima.commons.pojo.TimeLine;
import com.itheima.interfaces.PublishApi;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

@Service
public class PublishMapperImpl implements PublishApi {

    @Autowired
    private MongoTemplate mongoTemplate;


    @Override
    public ObjectId publishMessage(Publish publish) {
        Publish save = mongoTemplate.save(publish);

        return save.getId();
    }

    @Override
    public List<Publish> selectPublishId(Integer page, Integer pagesize, Long userId) {
        List<TimeLine> timeLines = mongoTemplate.find(new Query()
                        .with(PageRequest.of(page - 1, pagesize))
                        .with(Sort.by(Sort.Order.desc("date")))
                , TimeLine.class, "quanzi_time_line_" + userId);
        List<ObjectId> publishId = CollUtil.getFieldValues(timeLines, "publishId", ObjectId.class);
        List<Publish> publishes = mongoTemplate.find(Query.query(Criteria.where("id").in(publishId)), Publish.class);
        return publishes;
    }

    @Override
    public List<Publish> selectPublishByPid(List<Long> pid) {
        List<Publish> publishList = mongoTemplate.find(Query.query(Criteria.where("pid").in(pid)), Publish.class);

        return publishList;
    }

    @Override
    public List<Publish> randomUserPid(Integer pagesize) {
        AggregationResults<Publish> aggregate = mongoTemplate.aggregate(Aggregation.newAggregation(Publish.class, Aggregation.sample(pagesize)), Publish.class);
        List<Publish> mappedResults = aggregate.getMappedResults();
        return mappedResults;
    }

    @Override
    public Long likeComment(Comment comment) {
        Comment save = mongoTemplate.save(comment);
        Long count = findCount(save.getPublishId(), 1);
        return count;
    }

    @Override
    public Long NoLikeComment(ObjectId publishId, Long userId) {
        mongoTemplate.remove(Query.query(Criteria.where("publishId").is(publishId).and("userId").is(userId).and("commentType").is(1)), Comment.class);
        Long count = findCount(publishId, 1);
        return count;
    }

    @Override
    public Long loveComment(Comment comment) {
        Comment save = mongoTemplate.save(comment);
        Long count = findCount(save.getPublishId(), 3);
        return count;
    }

    @Override
    public Long NoLoveComment(ObjectId publishId, Long userId) {
        mongoTemplate.remove(Query.query(Criteria.where("publishId").is(publishId).and("userId").is(userId)), Comment.class);
        Long count = findCount(publishId, 3);
        return count;
    }

    @Override
    public Publish queryById(ObjectId id) {
        return mongoTemplate.findOne(Query.query(Criteria.where("id").is(id)), Publish.class);
    }

    @Override
    public Publish selectByPublishId(String publishId) {
        Publish publish = mongoTemplate.findOne(Query.query(Criteria.where("id").is(publishId)), Publish.class);
        return publish;
    }

    @Override
    public List<Publish> selectAlbumById(List<String> publish) {
        List<Publish> publishList = mongoTemplate.find(Query.query(Criteria.where("id").in(publish)), Publish.class);
        return publishList;
    }


    @Override
    public Long findCount(ObjectId publishId, Integer commentType) {
        long count = mongoTemplate.count(Query.query(Criteria.where("publishId")
                .in(publishId).and("commentType").is(commentType)), Comment.class);
        return count;
    }

    @Override
    public List<Publish> queryPublishes(List<ObjectId> publishIds) {
        return mongoTemplate.find(Query.query(Criteria.where("id").in(publishIds)), Publish.class);
    }
}
