package com.itheima.interfaces;

import com.itheima.commons.pojo.UserLike;
import com.itheima.commons.pojo.Users;
import com.itheima.commons.pojo.Visitors;

import java.util.List;

public interface UsersApi {
    /**
     * 添加到好友表
     * @param users
     */
    void saveUsers(Users users);



    /**
     * 根据自己的id 查询好友id
     *
     * @param page
     * @param pagesize
     * @param userId
     * @return
     */
    List<Users> selectFriendByUserId(Integer page, Integer pagesize, String keyword, Long userId);

    /**
     * 查询喜欢数
     * @param userId
     * @return
     */
    Long selectLove(Long userId);

    /**
     * 查询粉丝数
     * @param userId
     */
    Long selectFans(Long userId);

    /**
     * 查询自己喜欢人的id
     * @param userId
     * @return
     */
    List<Long> selectLoveId(Long userId);

    /**
     * 查询喜欢的人是否喜欢我
     * @param idList
     */
    Long selectEachLove(List<Long> idList,Long id);

    /**
     * 分页查询喜欢
     * @param userId
     * @param page
     * @param pagesize
     */
    List<Long> selectLovePage(Long userId, Integer page, Integer pagesize);

    /**
     * 分页查询粉丝
     * @param userId
     * @param page
     * @param pagesize
     * @return
     */
    List<Long> selectFansPage(Long userId, Integer page, Integer pagesize);

    /**
     * 查询我喜欢的人是否喜欢我
     * @param selectLoveId
     * @param page
     * @param pagesize
     * @return
     */
    List<Long> selectEachLovePage(List<Long> selectLoveId,Long userId ,Integer page, Integer pagesize);

    List<Long> visitors(Long userId, Integer page, Integer pagesize);

    /**
     * 是否喜欢他
     * @param userId
     * @param userId1
     */
    UserLike aleadyLove(Long userId, Long userId1);

    /**
     * 查询访问用户信息
     * @param userId
     * @return
     */
    List<Visitors> visitorsMyHome(Long userId,Long userTime);

    /**
     * 删除用户id
     * @param uid
     */
    void deleteUserId(Long id,Long uid);

    /**
     * 查询是否互相喜欢
     * @param userId
     * @param uid
     * @return
     */
    UserLike selectIsEachLove(Long userId, Long uid);

    /**
     * 添加到喜欢表
     * @param userLike
     */
    void saveUserLike(UserLike userLike);
}
