package com.itheima.manage.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.LocalDateTimeUtil;
import cn.hutool.core.math.MathUtil;
import cn.hutool.core.util.NumberUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.commons.pojo.User;
import com.itheima.commons.pojo.UserInfo;
import com.itheima.commons.pojo.UserInfoData;
import com.itheima.commons.vo.OperationLog;
import com.itheima.commons.vo.PostLoginDayVo;
import com.itheima.interfaces.*;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

@Service
public class PostManService {

    @Reference
    private UserApi userApi;


    @Reference
    private UserLoginApi userLoginApi;
    @Reference
    private UserTimeApi userTimeApi;
    @Reference
    private UserInfoDataApi userInfoDataApi;

    @Reference
    private OperationLogApi operationLogApi;

    @Autowired
    private AppManagePanelInfoService panelInfoService;

    @SneakyThrows
    public List<PostLoginDayVo> selectRegisterNum(String st, String et, Integer sex) {
        DateTime stDate = DateUtil.parse(st);
        DateTime etDate = DateUtil.parse(et);
        long between = LocalDateTimeUtil.between(LocalDateTimeUtil.of(stDate), LocalDateTimeUtil.of(etDate), ChronoUnit.DAYS);
        List<PostLoginDayVo> list = new ArrayList<>();
        for (long i = 0; i <= between; i++) {
            PostLoginDayVo postLoginDayVo = new PostLoginDayVo();

            DateTime time = nowDateTimeUp(stDate, i);
            DateTime dateTime = DateUtil.beginOfDay(time);
            DateTime endOfDay = DateUtil.endOfDay(time);
            // 新增用户登录数/新增用户数*100%
            //查询今日新增用户
            List<UserInfoData> count = userInfoDataApi.selectNowDayUser(dateTime + "", endOfDay + "", sex);
            String format = DateUtil.format(dateTime, "yyyy-MM-dd");
            List<Long> values = CollUtil.getFieldValues(count, "id", Long.class);
            Integer day1 = dayUserLogin(dateTime, endOfDay, 1L, values);
            Integer day2 = dayUserLogin(dateTime, endOfDay, 2L, values);
            Integer day3 = dayUserLogin(dateTime, endOfDay, 3L, values);
            Integer day4 = dayUserLogin(dateTime, endOfDay, 4L, values);
            Integer day5 = dayUserLogin(dateTime, endOfDay, 5L, values);
            Integer day6 = dayUserLogin(dateTime, endOfDay, 6L, values);
            Integer day7 = dayUserLogin(dateTime, endOfDay, 7L, values);
            Integer day30 = dayUserLogin(dateTime, endOfDay, 30L, values);
            postLoginDayVo.setDate(format);
            postLoginDayVo.setRegister(count.size());
            postLoginDayVo.setOneDay(NumberUtil.div(day1 * 1.0, count.size(), 2) + "%");
            postLoginDayVo.setTwoDay(NumberUtil.div(day2 * 1.0, count.size(), 2) + "%");
            postLoginDayVo.setThreeDay(NumberUtil.div(day3 * 1.0, count.size(), 2) + "%");
            postLoginDayVo.setFourDay(NumberUtil.div(day4 * 1.0, count.size(), 2) + "%");
            postLoginDayVo.setFiveDay(NumberUtil.div(day5 * 1.0, count.size(), 2) + "%");
            postLoginDayVo.setSixDay(NumberUtil.div(day6 * 1.0, count.size(), 2) + "%");
            postLoginDayVo.setSevenDay(NumberUtil.div(day7 * 1.0, count.size(), 2) + "%");
            postLoginDayVo.setThirtyDay(NumberUtil.div(day30 * 1.0, count.size(), 2) + "%");
            list.add(postLoginDayVo);
        }
        return list;
    }

    public DateTime nowDateTimeUp(DateTime time, Long i) {
        return new DateTime(time.getTime() + (1000 * 60 * 60 * 24 * i));
    }

    public Integer dayUserLogin(DateTime dateTime, DateTime endOfDay, Long i, List<Long> values) {
        DateTime oneDay1 = nowDateTimeUp(dateTime, i);
        DateTime oneDay2 = nowDateTimeUp(endOfDay, i);
        Integer count = userTimeApi.selectUserLogin(values, oneDay1.toString(), oneDay2.toString());
        return count;
    }

    public List<OperationLog> selectOperationLog(String st, String et, String name, String text) {
        List<OperationLog> list = operationLogApi.selectOperation(st, et, name, text);
        return list;
    }
}
