package com.itheima.manage.cheduler;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.thread.ThreadUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.commons.pojo.AuditState;
import com.itheima.commons.pojo.Publish;
import com.itheima.commons.utils.RunTextModerationSolution;
import com.itheima.interfaces.AuditStateApi;
import com.itheima.interfaces.PublishApi;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;

@Component
public class JobScheduled {

    @Reference
    private AuditStateApi auditStateApi;

    @Reference
    private PublishApi publishApi;


    @Scheduled(cron = "0/59 * * * * ? ")
    public void execute() {
        List<AuditState> auditStateList = auditStateApi.selectAuditState("1");

        if (CollUtil.isEmpty(auditStateList)) {
            return;
        }
        List<String> publishId = CollUtil.getFieldValues(auditStateList, "publishId", String.class);
        List<Publish> publishes = publishApi.selectAlbumById(publishId);
        for (Publish publish : publishes) {
            String text = publish.getText();
            String textCheck = RunTextModerationSolution.textCheck(text);
            List<String> medias = publish.getMedias();
            Integer integer = RunTextModerationSolution.imgCheck(medias);
            System.out.println(text);
            System.out.println(textCheck);
            if (textCheck.equals("pass") && integer == 200) {
                //通过    修改状态码
                auditStateApi.updateState(publish.getId().toString(), 2);
            } else if (textCheck.equals("block") || integer != 200) {
                //  有脏话 自动审核拒绝
                auditStateApi.updateState(publish.getId().toString(), 6);
            } else {
                // 转为人工
                auditStateApi.updateState(publish.getId().toString(), 3);
            }
        }
    }

}
