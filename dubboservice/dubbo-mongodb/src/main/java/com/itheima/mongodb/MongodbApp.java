package com.itheima.mongodb;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@MapperScan("com.itheima.mongodb.mapper")
@EnableAsync
public class MongodbApp {
    public static void main(String[] args) {
        SpringApplication.run(MongodbApp.class,args);
    }
}
