package com.itheima.commons.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class VideoLogVo implements Serializable {

    private Long id;
    private Long userId;
    private String nickname;
    private String videoUrl;
    private String picUrl;
    private Long likeCount;
    private Long reportCount;
    private Long commentCount;
    private Long forwardingCount;
    private Long createDate;

}