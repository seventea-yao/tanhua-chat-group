package com.itheima.mysql.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.commons.vo.OperationLog;
import com.itheima.interfaces.OperationLogApi;

public interface OperationLogMapper extends BaseMapper<OperationLog> {
}
