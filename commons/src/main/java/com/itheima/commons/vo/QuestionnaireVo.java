package com.itheima.commons.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class QuestionnaireVo {
    private String id;
    private String name;
    private Integer star;
    private String cover;
    private Integer isLock;
    private String level;
    //试题
    private List<QuestionsVo> questions;
}
