package com.itheima.commons.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SoundVo implements Serializable {
    //用户id
    private Integer id;
    //头像
    private String avatar;
    //昵称
    private String nickname;
    //性别 man woman
    private String gender;
    //年龄
    private Integer age;
    //语音地址
    private String soundUrl;
    //剩余次数
    private  Integer remainingTimes;
}
