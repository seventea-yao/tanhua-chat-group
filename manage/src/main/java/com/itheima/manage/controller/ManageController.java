package com.itheima.manage.controller;

import cn.hutool.core.convert.Convert;
import com.itheima.commons.pojo.Freeze;
import com.itheima.commons.vo.BasicUserInfoVo;
import com.itheima.commons.vo.MessagesVo;
import com.itheima.commons.vo.PageResult;
import com.itheima.commons.vo.MessagesPageResultPuls;
import com.itheima.manage.service.ManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("manage")
public class ManageController {
    @Autowired
    private ManageService manageService;

    /**
     * 用户数据翻页
     *
     * @param
     * @return private String pagesize;//开始时间
     * private String page;//结束时间
     * private String id;//用户ID
     * private String nickname;//昵称
     * private String city;//注册城市名称
     */
    @GetMapping("users")
    public ResponseEntity queryUsers(@RequestParam(value = "page", defaultValue = "1", required = false) Integer page,
                                     @RequestParam(value = "pagesize", defaultValue = "10", required = false) Integer pagesize,
                                     @RequestParam(value = "nickname", required = false) String nickname,
                                     @RequestParam(value = "city", required = false) String city,
                                     @RequestParam(value = "id", required = false) Long id) {

        PageResult pageResult = manageService.queryUsersByQueryUserInfoConditions(page, pagesize, id, nickname, city);
        //返回值PageResult
        return ResponseEntity.ok(pageResult);
    }

    /**
     * 用户冻结操作
     *
     * @param freeze 请求参数
     * @return
     */
    @PostMapping("users/freeze")
    public ResponseEntity addFreeze(@RequestBody Freeze freeze) {
        Boolean message = manageService.addFreeze(freeze);
        //返回值键值对message=“message”
        return ResponseEntity.ok(message);
    }

    /**
     * 用户解冻操作
     * <p>
     * userId 被操作用户ID
     * reasonsForThawing 解冻原因
     *
     * @return
     */
    @PostMapping("users/unfreeze")
    public ResponseEntity unfreeze(@RequestBody Map<String, String> map) {
        Integer userId = Convert.toInt(map.get("userId"));
        String reasonsForThawing = map.get("reasonsForThawing");
        Boolean message = manageService.deleteFreezeByUserId(userId, reasonsForThawing);
        //返回值键值对message=“操作成功” message ? "message=操作成功" : "message=操作失败"
        return ResponseEntity.ok(message);
    }

    /**
     * 用户基本资料
     *
     * @param userId 被查询Id
     * @return
     */
    @GetMapping("users/{userID}")
    public ResponseEntity queryUsersByuserId(@PathVariable("userID") String userId) {

        BasicUserInfoVo basicUserInfoVo = manageService.queryBasicUserInfoByUserId(userId);

        return ResponseEntity.ok(basicUserInfoVo);
    }

    /**
     * 消息翻页
     *
     * @param page      页尺寸
     * @param pagesize  页数
     * @param sortProp  排序字段
     * @param sortOrder ascending 升序 descending 降序
     * @param id        消息id
     * @param sd        开始时间
     * @param ed        结束时间
     * @param uid       用户ID
     * @param state     审核状态
     * @return
     */
    @GetMapping("messages")
    public ResponseEntity queryMessages(@RequestParam(value = "page", defaultValue = "1", required = false) Integer page,
                                        @RequestParam(value = "pagesize", defaultValue = "10", required = false) Integer pagesize,
                                        @RequestParam(value = "sortProp", required = false) String sortProp,
                                        @RequestParam(value = "sortOrder", required = false) String sortOrder,
                                        @RequestParam(value = "id", required = false) Long id,
                                        @RequestParam(value = "sd", required = false) Long sd,
                                        @RequestParam(value = "ed", required = false) Long ed,
                                        @RequestParam(value = "uid", required = false) Long uid,
                                        @RequestParam(value = "state", required = false) String state) {

        MessagesPageResultPuls pageResultPuls = manageService.queryMessages(page, pagesize, sortProp, sortOrder, id, sd, ed, uid, state);

        //返回值PageResultPlus
        return ResponseEntity.ok(pageResultPuls);
    }


    /**
     * 消息详情
     *
     * @param id
     * @return
     */
    @GetMapping("messages/{id}")
    public ResponseEntity queryMessageById(@PathVariable("id") String id) {
        MessagesVo messagesVo = manageService.queryMessageById(id);

        return ResponseEntity.ok(messagesVo);
    }

    /**
     * 消息置顶
     *
     * @param id 被操作动态id
     * @return
     */
    @PostMapping("messages/{id}/top")
    public ResponseEntity setAuditStateTop(@PathVariable String id) {
        //返回执行结果
        Boolean result = manageService.setAuditStateTop(id,2);//置顶状态，1为未置顶，2为置顶
        return ResponseEntity.ok(result);
    }

    /**
     * 取消消息置顶
     *
     * @param id 被操作动态id
     * @return
     */
    @PostMapping("messages/{id}/untop")
    public ResponseEntity setAuditStateCancelTop(@PathVariable String id) {
        //返回执行结果
        Boolean result = manageService.setAuditStateTop(id,1);//置顶状态，1为未置顶，2为置顶
        return ResponseEntity.ok(result);
    }

    /**
     * 消息通过
     * 审核状态，1为待审核，2为自动审核通过，3为待人工审核，5为人工审核拒绝，4为人工审核通过，6为自动审核拒绝
     * @param strings 被操作动态id集合
     * @return
     */
    @PostMapping("messages/pass")
    public ResponseEntity setAuditStatePass(@RequestBody String[] strings) {
        //返回执行结果
        Boolean result = manageService.revocation(strings,"4");
        return ResponseEntity.ok(result);
    }

    /**
     * 消息拒绝
     * 审核状态，1为待审核，2为自动审核通过，3为待人工审核，5为人工审核拒绝，4为人工审核通过，6为自动审核拒绝
     * @param strings 被操作动态id集合
     * @return
     */
    @PostMapping("messages/reject")
    public ResponseEntity setAuditStateReject(@RequestBody String[] strings) {
        //返回执行结果
        Boolean result = manageService.revocation(strings,"5");
        return ResponseEntity.ok(result);
    }

    /**
     * 消息撤销
     * 审核状态，1为待审核，2为自动审核通过，3为待人工审核，5为人工审核拒绝，4为人工审核通过，6为自动审核拒绝
     * @param publishIds
     * @return
     */
    @PostMapping("messages/revocation")
    public ResponseEntity revocation(@RequestBody String[] publishIds) {
        //返回执行结果
        Boolean result = manageService.revocation(publishIds,"3");
        return ResponseEntity.ok(result);
    }

    //用户管理-用户详情-视频信息
    @GetMapping("videos")
    public ResponseEntity videos(@RequestParam("page") Integer page, @RequestParam("pagesize") Integer pageSize,
                                 @RequestParam("sortProp") String sortProp, @RequestParam("sortOrder") String sortOrder,
                                 @RequestParam("uid") Long uid) {
        PageResult pageResult = manageService.userVideoLogs(page, pageSize, sortProp, sortOrder, uid);
        return ResponseEntity.ok(pageResult);
    }

    //用户管理-用户详情-登录信息
    @GetMapping("logs")
    public ResponseEntity logs(@RequestParam("page") Integer page, @RequestParam("pagesize") Integer pageSize,
                               @RequestParam("sortProp") String sortProp, @RequestParam("sortOrder") String sortOrder,
                               @RequestParam("uid") Long uid) {
        PageResult pageResult = manageService.userLoginLogs(page, pageSize, sortProp, sortOrder, uid);
        return ResponseEntity.ok(pageResult);
    }

    /**
     * 评论列表
     * @param page
     * @param pageSize
     * @param sortProp
     * @param sortOrder
     * @param messageID
     * @return
     */
    @GetMapping("messages/comments")
    public ResponseEntity comments(@RequestParam("page") Integer page, @RequestParam("pagesize") Integer pageSize,
                                   @RequestParam("sortProp") String sortProp, @RequestParam("sortOrder") String sortOrder,
                                   @RequestParam("messageID") String messageID){
        PageResult pageResult = manageService.userCommentsLogs(page,pageSize,sortProp,sortOrder,messageID);
        return ResponseEntity.ok(pageResult);
    }
}
