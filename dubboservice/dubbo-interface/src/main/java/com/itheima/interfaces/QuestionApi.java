package com.itheima.interfaces;

import com.itheima.commons.pojo.Question;

public interface QuestionApi {
    /**
     * 根据用户id 查询用户问题
     * @param id
     * @return
     */
    Question strangerQuestions(Long id);

    /**
     * 保存问题
     */
    void saveQuestion(Question question);

    /**
     * 修改问题
     * @param userId
     * @param content
     */
    void updateQuestion(Long userId, String content);
}
