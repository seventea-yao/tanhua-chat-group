package com.itheima.mysql.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.commons.pojo.Conclusion;

public interface ConclusionMapper extends BaseMapper<Conclusion> {
}
