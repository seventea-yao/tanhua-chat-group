package com.itheima.appserver.interceptor;

import com.itheima.commons.pojo.User;

public class UserThreadLocal {
    public static final ThreadLocal<User> THREAD_LOCAL = new ThreadLocal<>();


    public static void setUser(User user){
        THREAD_LOCAL.set(user);
    }

    public static Long getUserId(){
        return THREAD_LOCAL.get().getId();
    }


    public static String getUserPhone(){
        return THREAD_LOCAL.get().getMobile();
    }

    public static void remover(){
        THREAD_LOCAL.remove();
    }
}
