package com.itheima.interfaces;

import com.itheima.commons.pojo.HuanxinUser;

public interface HuanXinApi {

    void save(HuanxinUser huanXin);

    HuanxinUser findHuanXinUser(Long userId);
}
