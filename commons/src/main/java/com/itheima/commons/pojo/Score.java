package com.itheima.commons.pojo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Score implements Serializable {
    private Long id;
    private Long QuestionsId;
    private Long optionId;
    private Long score;
}
