package com.itheima.appserver;

import cn.hutool.core.collection.CollStreamUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.stream.StreamUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@SpringBootTest(classes = AppServerApp.class)
@RunWith(SpringRunner.class)
public class App {

    @Autowired
    private Retry retryService;

    @Test
    public void testRetry() {
        System.out.println(this.retryService.retry(90));
    }


    @Test
    public void test1() {
        String[] strArr = {"abcd", "bcdd", "defde", "fTr"};
        List<String> collect = Arrays.stream(strArr).map(String::toUpperCase).collect(Collectors.toList());
        List<Integer> intList = Arrays.asList(1, 3, 5, 7, 9, 11);
        List<Boolean> collect1 = intList.stream().map(x -> x > 3).collect(Collectors.toList());

    }
}