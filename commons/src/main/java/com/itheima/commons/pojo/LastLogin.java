package com.itheima.commons.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
//用户上次登陆时间
public class LastLogin implements Serializable {
    private Long id;
    private Long userId;//用户Id
    private Date lastLoginTime;//上次登陆时间
}
