package com.itheima.interfaces;

import com.itheima.commons.pojo.TimeLine;
import org.bson.types.ObjectId;

import java.util.List;

public interface TimeLineApi {
    //查询自己的时间线表
    List<Long> selectTimeLine(Long userId);

    //往好友的时间线表添加自己的朋友圈id
    void savePublishMessage(TimeLine timeLine,List<Long> longs);

}
