package com.itheima.appserver.service.impl;
import com.itheima.appserver.interceptor.UserThreadLocal;
import com.itheima.commons.vo.UserLocationVo;
import org.elasticsearch.common.geo.GeoPoint;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.appserver.service.BaiDuService;
import com.itheima.commons.pojo.UserLocation;
import com.itheima.interfaces.BaiDuApi;
import org.springframework.stereotype.Service;

@Service
public class BaiDuServiceImpl implements BaiDuService {

    @Reference
    private BaiDuApi baiDuApi;

    @Override
    public void location(Double latitude, Double longitude, String addrStr) {

       Boolean flag = baiDuApi.saveLocation(latitude,longitude,addrStr,UserThreadLocal.getUserId());
    }
}
