package com.itheima.commons.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Lock implements Serializable {
    private Long id;
    private Integer primarys;
    private Integer intermediate;
    private Integer senior;
    private Long uid;
}
