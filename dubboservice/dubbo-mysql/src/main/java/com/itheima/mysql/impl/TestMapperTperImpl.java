package com.itheima.mysql.impl;

import cn.hutool.core.convert.Convert;
import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.itheima.commons.pojo.*;
import com.itheima.commons.vo.OptionVo;
import com.itheima.interfaces.TestApi;
import com.itheima.mysql.mapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Service
public class TestMapperTperImpl implements TestApi {
    @Autowired
    QuestionnaireMapper quesqtionnaire;
    @Autowired
    QuestionsTestMapper questionsTestMapper;
    @Autowired
    OptionMapper optionMapper;
    @Autowired
    ScoreMapper scoreMapper;
    @Autowired
    LikeMapper likeMapper;
    @Autowired
    ConclusionMapper conclusionMapper;
    @Autowired
    LockMapper lockMapper;



    @Override
    public List<Questionnaire> findQuestionnaire() {
        List<Questionnaire> questionnaires = quesqtionnaire.selectList(Wrappers.lambdaQuery(Questionnaire.class));
        return questionnaires;
    }

    @Override
    public List<QuestionsTest> findQusetions() {
        List<QuestionsTest> Questions = questionsTestMapper.selectList(Wrappers.lambdaQuery(QuestionsTest.class));
        return Questions;
    }
    @Override
    public Map<Long, List<OptionVo>> findOption(List<Long> ids) {
        Map<Long, List<OptionVo>> data = new HashMap<>();
        for (Long id : ids) {
            ArrayList<OptionVo> optionVos = new ArrayList<>();
            List<Options> options = optionMapper.selectList(Wrappers.lambdaQuery(Options.class).in(Options::getQuestionId, id));
            for (Options option : options) {
                OptionVo optionVo = new OptionVo();
                optionVo.setId(option.getId().toString());
                optionVo.setOption(option.getOptions());
                optionVos.add(optionVo);
            }
            data.put(id,optionVos);
        }

        return data;
    }

    @Override
    public Long findScore(String optionId, String questionId) {
        Score score = scoreMapper.selectOne(Wrappers.lambdaQuery(Score.class).eq(Score::getOptionId, optionId).eq(Score::getQuestionsId, questionId));

        return score.getScore();
    }

    @Override
    public void saveReport(Long userId, int reportId) {
        Like like = new Like();
        like.setId(userId);
        like.setReportId(Convert.toLong(reportId));
        likeMapper.insert(like);
    }

    @Override
    public Conclusion findConclusion(String id) {
        Conclusion conclusion = conclusionMapper.selectById(id);
        return conclusion;
    }

    @Override
    public List<Like> findUser(String id) {
        List<Like> likes = likeMapper.selectList(Wrappers.lambdaQuery(Like.class).in(Like::getReportId, id));

        return likes;
    }

    @Override
    public Lock findLock(Long userId) {
        Lock lock = lockMapper.selectOne(Wrappers.lambdaQuery(Lock.class).eq(Lock::getUid,userId));

        return lock;
    }

    @Override
    public void updateLock(Lock lock) {
        lockMapper.update(lock,Wrappers.lambdaQuery(Lock.class).eq(Lock::getUid,lock.getUid()));
    }

    @Override
    public void saveLock(Lock lock1) {
        lockMapper.insert(lock1);
    }

}
