package com.itheima.manage.controller;

import com.itheima.commons.vo.DistributionVo;
import com.itheima.commons.vo.ProfileInformationVo;
import com.itheima.commons.vo.UserProportionVo;
import com.itheima.manage.service.AppManagePanelInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("dashboard")
public class PanelInfoController {

    @Autowired
    AppManagePanelInfoService appDashboardService;

    @GetMapping("summary")
    public ResponseEntity summary() {
        ProfileInformationVo data = appDashboardService.getSummary();
        return ResponseEntity.ok(data);
    }

    @GetMapping("users")
    public ResponseEntity users(@RequestParam("sd") Long sd,
                                @RequestParam("ed") Long ed,
                                @RequestParam("type") String type) {
        Map<String, Object> data = appDashboardService.getUsers(sd, ed, type);
        return ResponseEntity.ok(data);
    }

    @GetMapping("distribution")
    public ResponseEntity distribution(@RequestParam("sd") Long sd,
                                       @RequestParam("ed") Long ed) {
        Map<String, Object> data = appDashboardService.getDistribution(sd, ed);
        return ResponseEntity.ok(data);
    }
}
