package com.itheima.mysql.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.itheima.commons.pojo.HuanxinUser;
import com.itheima.interfaces.HuanXinApi;
import com.itheima.mysql.mapper.HuanXinMapper;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class HuanXinMapperImpl implements HuanXinApi {
    @Autowired
    private HuanXinMapper huanXinMapper;

    @Override
    public void save(HuanxinUser huanXin) {
        huanXinMapper.insert(huanXin);
    }

    @Override
    public HuanxinUser findHuanXinUser(Long userId) {
        HuanxinUser huanxinUser = huanXinMapper.selectOne(Wrappers.lambdaQuery(HuanxinUser.class).eq(HuanxinUser::getUserId, userId));
        return huanxinUser;
    }
}
