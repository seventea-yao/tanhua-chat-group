package com.itheima.commons.vo;
import com.itheima.commons.pojo.Dimensions;
import com.itheima.commons.pojo.SimilarYou;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ResultVo implements Serializable {
    //鉴定结果
    private String conclusion;
    //封面
    private String cover;
    //维度值
    private List<Dimensions> dimensions;
    //与你相似
    private List<SimilarYou> similarYou;

}


