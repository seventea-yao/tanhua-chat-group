package com.itheima.commons.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ManageUser  implements Serializable {

    //主键Id
    private Long id;

    private String username;

    private String password;
    //创建时间
    private Date created;
    //更新时间
    private Date updated;
}
