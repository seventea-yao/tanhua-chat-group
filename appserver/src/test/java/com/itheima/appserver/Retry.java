package com.itheima.appserver;

import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class Retry {

    @Retryable(value = RuntimeException.class,
            maxAttempts = 3,
            backoff = @Backoff(delay = 200L,maxDelay = 3))
    public  int retry(int num){
        Random random = new Random();
        int i = random.nextInt(99);
        if (num>i){
            throw  new RuntimeException();
        }
        return i;
    }

    @Recover
    public int recover(Exception e){
        System.out.println(555555);
        return 88;
    }
}
