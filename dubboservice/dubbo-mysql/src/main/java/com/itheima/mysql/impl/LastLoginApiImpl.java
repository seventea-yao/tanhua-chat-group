package com.itheima.mysql.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.commons.pojo.LastLogin;
import com.itheima.interfaces.LastLoginApi;
import com.itheima.mysql.mapper.LastLoginMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service
public class LastLoginApiImpl implements LastLoginApi {

    @Autowired
    private LastLoginMapper lastLoginMapper;

    @Override
    public void addLastLogin(LastLogin lastLogin) {
        lastLoginMapper.insert(lastLogin);
    }

    @Override
    public LastLogin findLastLoginByUserId(Long userId) {
        return lastLoginMapper.selectOne(Wrappers.lambdaQuery(LastLogin.class).eq(LastLogin::getUserId, userId));
    }

    @Override
    public Page<LastLogin> queryLastLogins(Integer page, Integer pagesize) {
        //分页查询 降序查询
        Page<LastLogin> lastLoginPage = lastLoginMapper.selectPage(new Page<>(page, pagesize),
                Wrappers.lambdaQuery(LastLogin.class).orderByDesc(LastLogin::getLastLoginTime)
        );

        return lastLoginPage;
    }

    @Override
    public List<LastLogin> queryLastLoginList(List<Long> userIds) {
        return lastLoginMapper.selectList(Wrappers.lambdaQuery(LastLogin.class).in(LastLogin::getUserId, userIds));
    }
}
