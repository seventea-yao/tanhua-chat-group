package com.itheima.appserver.controller;

import com.itheima.appserver.annotation.MyCache;
import com.itheima.appserver.service.PublishService;
import com.itheima.commons.vo.PageResult;
import com.itheima.commons.vo.QuanZiVo;
import com.itheima.commons.vo.VisitorsVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("movements")
public class PublishController {

    @Autowired
    private PublishService publishService;



    @PostMapping()
    public ResponseEntity movements(@RequestParam("textContent") String textContent,
                                    @RequestParam("location") String location,
                                    @RequestParam("longitude") String longitude,
                                    @RequestParam("latitude") String latitude,
                                    @RequestParam("imageContent") MultipartFile multipartFile[]) {
        publishService.publishMessage(textContent, location, longitude, latitude, multipartFile);
        return ResponseEntity.ok(null);
    }

    @GetMapping
    @MyCache(time = 45L)
    public ResponseEntity movements(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                    @RequestParam(value = "pagesize", defaultValue = "10") Integer pagesize) {
        PageResult pageResult = publishService.findFriendMovements(page, pagesize);
        return ResponseEntity.ok(pageResult);

    }

    @GetMapping("recommend")
    @MyCache(time = 45L)
    public ResponseEntity recommend(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                    @RequestParam(value = "pagesize", defaultValue = "10") Integer pagesize) {
        PageResult pageResult = publishService.findRecommendFriend(page, pagesize);
        return ResponseEntity.ok(pageResult);
    }


    @GetMapping("{id}/like")
    public ResponseEntity like(@PathVariable("id") String id) {
        Long commentCount = publishService.likeComment(id);
        return ResponseEntity.ok(commentCount);
    }


    @GetMapping("{id}/dislike")
    public ResponseEntity noLike(@PathVariable("id") String id) {
        Long commentCount = publishService.noLikeComment(id);
        return ResponseEntity.ok(commentCount);
    }


    @GetMapping("{id}/love")
    public ResponseEntity love(@PathVariable("id") String id) {
        Long commentCount = publishService.loveComment(id);
        return ResponseEntity.ok(commentCount);
    }

    @GetMapping("{id}/unlove")
    public ResponseEntity NoLove(@PathVariable("id") String id) {
        Long commentCount = publishService.noLoveComment(id);
        return ResponseEntity.ok(commentCount);
    }

    /**
     * 查询单条动态信息
     *
     * @param id
     * @return
     */
    @GetMapping("{id}")
    public ResponseEntity queryById(@PathVariable("id") String id) {
        QuanZiVo quanZiVo = publishService.queryById(id);
        return ResponseEntity.ok(quanZiVo);
    }

    /**
     * 谁看过我
     * movements/visitors
     * @return
     */
    @GetMapping("visitors")
    public ResponseEntity visitors() {
        List<VisitorsVo> visitorsVoList =publishService.visitors();
        return ResponseEntity.ok(visitorsVoList);
    }

    /**
     * 查询用户动态
     *
     * @param page
     * @param pagesize
     * @param userId   要查询用户的id
     * @return
     */
    @GetMapping("all")
    public ResponseEntity movementsAll(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                       @RequestParam(value = "pagesize", defaultValue = "10") Integer pagesize,
                                       @RequestParam("userId") Long userId) {
        PageResult pageResult = publishService.queryAlbumList(page, pagesize, userId);
        return ResponseEntity.ok(pageResult);
    }
}
