package com.itheima.mysql.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.commons.pojo.UserTime;

public interface UserTimeMapper extends BaseMapper<UserTime> {
}
