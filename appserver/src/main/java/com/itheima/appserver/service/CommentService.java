package com.itheima.appserver.service;

import com.itheima.commons.vo.CommentVo;
import com.itheima.commons.vo.PageResult;
import org.springframework.data.domain.PageRequest;

import java.util.Map;

public interface CommentService {
    PageResult queryCommentsList(String publishId, Integer page, Integer pagesize);

    Long likeComment(String publishId);

    void disLikeComment(String publishId);

    void saveComment(String movementId, String comment);
}
