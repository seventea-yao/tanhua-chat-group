package com.itheima.mysql.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.commons.pojo.UserInfoData;

public interface UserInfoDataMapper extends BaseMapper<UserInfoData> {
}
