package com.itheima.appserver.service.impl;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.appserver.interceptor.UserThreadLocal;
import com.itheima.appserver.service.HuanXinService;
import com.itheima.commons.pojo.HuanxinUser;
import com.itheima.interfaces.HuanXinApi;
import org.springframework.stereotype.Service;

@Service
public class HuanXinServiceImpl implements HuanXinService {

    @Reference
    private HuanXinApi huanXinApi;


    @Override
    public HuanxinUser findHuanXinUser() {
        HuanxinUser huanxinUser= huanXinApi.findHuanXinUser(UserThreadLocal.getUserId());
        return huanxinUser;
    }
}
