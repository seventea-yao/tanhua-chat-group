package com.itheima.mysql.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.itheima.commons.pojo.Announcement;
import com.itheima.interfaces.AnnouncementApi;
import com.itheima.mysql.mapper.AnnouncementMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service
public class AnnouncementMapperImpl implements AnnouncementApi {


    @Autowired
    private AnnouncementMapper announcementMapper;

    @Override
    public List<Announcement> selectAnnouncement() {
        List<Announcement> announcements = announcementMapper.selectList(Wrappers.lambdaQuery(Announcement.class));
        return announcements;
    }
}
