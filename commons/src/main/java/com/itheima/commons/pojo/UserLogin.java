package com.itheima.commons.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserLogin implements Serializable {

    private Long id;
    private Long userId;
    //登录时间
    private Date loginTime;
    //登出时间
    private Date outTime;
}
