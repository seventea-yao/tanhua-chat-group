package com.itheima.commons.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
//图型数据
public class LinearVo implements Serializable {

    private String title;
    private Integer amount;
}