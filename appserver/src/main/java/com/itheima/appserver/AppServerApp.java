package com.itheima.appserver;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.scheduling.annotation.EnableScheduling;
@SpringBootApplication(exclude = MongoAutoConfiguration.class)
@EnableRetry
@EnableScheduling
public class AppServerApp {
    public static void main(String[] args) {
        SpringApplication.run(AppServerApp.class,args);
    }
}
