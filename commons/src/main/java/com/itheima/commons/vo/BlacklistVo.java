package com.itheima.commons.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class BlacklistVo implements Serializable {

    private Integer id;
    private String avatar;
    private String nickname;
    private String gender;
    private Integer age;
}
