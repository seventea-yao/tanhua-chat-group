package com.itheima.appserver.service.impl;

import com.itheima.commons.constants.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class PidService {
    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    public  Long pidPro(String type) {
        String pid = Constants.TANHUA_UNIT_ID+type;
        return redisTemplate.opsForValue().increment(pid);
    }
}
