package com.itheima.commons.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CommentsVo implements Serializable {
    private String id;
    private String nickname;
    private Integer userId;
    private String content;
    private Integer createDate;

}
