package com.itheima.commons.pojo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "sound")
public class Sound implements Serializable {
    private ObjectId id; //主键id
    private Long userId;
    private String soundUrl; //音频文件
    private Long created; //创建时间


}
