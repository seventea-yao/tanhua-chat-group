package com.itheima.mysql.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.commons.pojo.Score;

public interface ScoreMapper extends BaseMapper<Score> {
}
