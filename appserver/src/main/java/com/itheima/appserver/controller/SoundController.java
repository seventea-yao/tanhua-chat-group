package com.itheima.appserver.controller;
import com.itheima.appserver.service.SoundService;
import com.itheima.commons.vo.SoundVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
@RestController
@RequestMapping("peachblossom")
public class SoundController {
    //发送语音
    @Autowired
    private SoundService soundService;
    @PostMapping
    public ResponseEntity sendSound(@RequestParam("soundFile") MultipartFile multipartFile) {
        soundService.sendSound(multipartFile);
        return ResponseEntity.ok(null);
    }
    //接收语音
    @GetMapping
    public ResponseEntity receiveSound() {
       SoundVo soundVo= soundService.receiveSound();
        return ResponseEntity.ok(soundVo);
    }
}
