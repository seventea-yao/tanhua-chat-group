package com.itheima.appserver.service;

import com.itheima.commons.vo.NearUserVo;
import com.itheima.commons.vo.PageResult;
import com.itheima.commons.vo.TodayBest;

import java.util.List;
import java.util.Map;

public interface TanHuaService {
    TodayBest todayBest();

    PageResult recommendation(Map<String, Object> map);

    TodayBest personalInfo(Long id);

    String strangerQuestions(Long id);

    void answerQuestion(Object userId, Object reply);

    List<NearUserVo> search(String gender, Double distance);

    List<TodayBest> selectCards();

    void loveCard(Long id);

    void unloveCard(Long id);
}
