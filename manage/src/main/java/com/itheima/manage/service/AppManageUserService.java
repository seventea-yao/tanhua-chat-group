package com.itheima.manage.service;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.jwt.JWTPayload;
import cn.hutool.jwt.JWTUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.commons.constants.Constants;
import com.itheima.commons.pojo.ManageUser;
import com.itheima.commons.pojo.UserInfo;
import com.itheima.commons.utils.DateTimeUtil;
import com.itheima.commons.vo.ProfileVo;
import com.itheima.interfaces.ManageUserApi;
import com.itheima.interfaces.UserInfoApi;
import com.itheima.interfaces.UserTimeApi;
import com.itheima.manage.exception.MyException;
import com.itheima.manage.exception.ResultError;
import com.itheima.manage.interceptor.UserThreadLocal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class AppManageUserService {

    @Reference
    private ManageUserApi ManageUserApi;

    @Reference
    private UserTimeApi userTimeApi;

    @Reference
    private UserInfoApi userInfoApi;

    @Autowired
    RedisTemplate<String, String> redisTemplate;

    public Map<String, Object> login(Map<String, String> map) {
        String username = map.get("username");
        String password = map.get("password");
        String verificationCode = map.get("verificationCode");
        String uuid = map.get("uuid");
        String code = redisTemplate.opsForValue().get("IMAGE_CODE" + uuid);
        if (StrUtil.isEmpty(code)) {
            throw new MyException(ResultError.codeError());
        }

        if (!code.equals(verificationCode)) {
            throw new MyException(ResultError.codeError());
        }

        ManageUser manageUser = ManageUserApi.login(username, password);
        if (ObjectUtil.isEmpty(manageUser)) {
            throw new MyException(ResultError.UserInfoError());
        }
        Map<String, Object> date = new HashMap<>();
        //设置token
        Map<String, Object> jwtMap = new HashMap<>();
        jwtMap.put("username", manageUser.getUsername());
        jwtMap.put("uid", manageUser.getId());
        jwtMap.put(JWTPayload.EXPIRES_AT, DateTime.of(System.currentTimeMillis() + Constants.JWT_TIME_OUT));
        // 载荷信息，密钥信息
        String token = JWTUtil.createToken(jwtMap, Constants.JWT_SECRET.getBytes());
        date.put("token", token);
        // 记录登录时间
        userTimeApi.saveLoginTime(manageUser.getId(), new Date());
        System.out.println(token);
        return date;
    }

    public ProfileVo profile() {
        //查询用户头像
        UserInfo userInfo = userInfoApi.findUserInfoByUserId(UserThreadLocal.getUserId());
        ProfileVo profileVo = new ProfileVo();
        profileVo.setUid(Convert.toStr(UserThreadLocal.getUserId()));
        profileVo.setUsername(Convert.toStr(UserThreadLocal.getUsername()));
        profileVo.setAvatar(userInfo.getLogo());
        System.out.println(UserThreadLocal.getUserId() + "," + UserThreadLocal.getUsername());
        return profileVo;
    }

    public void logout() {
        UserThreadLocal.remover();
    }
}
