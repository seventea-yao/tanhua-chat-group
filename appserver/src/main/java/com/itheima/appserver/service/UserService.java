package com.itheima.appserver.service;

import java.util.HashMap;
import java.util.Map;

public interface UserService {
    /**
     *
     * @param phone
     */
    public void userSendLoginMsg(String phone);

    HashMap<String, Object> userLoginVerification(Map<String, String> map);
}
