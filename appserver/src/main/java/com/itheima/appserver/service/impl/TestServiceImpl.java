package com.itheima.appserver.service.impl;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.appserver.interceptor.UserThreadLocal;
import com.itheima.appserver.service.TestService;
import com.itheima.commons.constants.Constants;
import com.itheima.commons.pojo.*;
import com.itheima.commons.vo.OptionVo;
import com.itheima.commons.vo.QuestionnaireVo;
import com.itheima.commons.vo.QuestionsVo;
import com.itheima.commons.vo.ResultVo;
import com.itheima.interfaces.TestApi;
import com.itheima.interfaces.UserInfoApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
@Service
public class TestServiceImpl implements TestService {
    @Reference
    TestApi testApi;
    @Autowired
    RedisTemplate<String, String> redisTemplate;
    @Reference
    UserInfoApi userInfoApi;

    public List<QuestionnaireVo> sendSound() {
        //查询锁的状tai
        Lock lock = testApi.findLock(UserThreadLocal.getUserId());
        if (ObjectUtil.isEmpty(lock)) {
            Lock lock1 = new Lock();
            lock1.setPrimarys(0);
            lock1.setIntermediate(1);
            lock1.setSenior(1);
            lock1.setUid(UserThreadLocal.getUserId());
            testApi.saveLock(lock1);
            lock = testApi.findLock(UserThreadLocal.getUserId());
            redisTemplate.opsForValue().set(Constants.QUESTIONNAIRE_INTERMEDIATE + UserThreadLocal.getUserId(), "1");
            redisTemplate.opsForValue().set(Constants.QUESTIONNAIRE_SENIOR + UserThreadLocal.getUserId(), "1");
        }
        ArrayList<QuestionnaireVo> arrayList = new ArrayList<>();
        //问卷
        List<Questionnaire> optionList = testApi.findQuestionnaire();
        //题目对象vo
        ArrayList<QuestionsVo> qusetions = new ArrayList<>();
        //题目对象
        List<QuestionsTest> questionsTests = testApi.findQusetions();
        List<Long> ids = CollUtil.getFieldValues(questionsTests, "id", Long.class);
        //选项
        Map<Long, List<OptionVo>> option = testApi.findOption(ids);
        for (QuestionsTest questionsTest : questionsTests) {
            QuestionsVo questionsVo = new QuestionsVo();
            questionsVo.setId(questionsTest.getId());
            questionsVo.setQuestion(questionsTest.getQuestion());
            questionsVo.setOptions(option.get(Convert.toLong(questionsTest.getId())));
            qusetions.add(questionsVo);
        }
        for (Questionnaire questionnaire : optionList) {
            QuestionnaireVo questionnaireVo = new QuestionnaireVo();
            questionnaireVo.setId(questionnaire.getId());
            questionnaireVo.setName(questionnaire.getName());
            questionnaireVo.setStar(Convert.toInt(questionnaire.getStar()));
            questionnaireVo.setCover(questionnaire.getCover());
            if (questionnaire.getId().equals("1")) {
                questionnaireVo.setIsLock(lock.getPrimarys());
            } else if (questionnaire.getId().equals("2")) {
                questionnaireVo.setIsLock(lock.getIntermediate());
            } else if (questionnaire.getId().equals("3")) {
                questionnaireVo.setIsLock(lock.getSenior());
            }
            questionnaireVo.setLevel(questionnaire.getLevel());
            questionnaireVo.setQuestions(qusetions);
            arrayList.add(questionnaireVo);
        }

        return arrayList;
    }

    @Override
    public String testSubmit(List<Answers> answers) {
        //String index = redisTemplate.opsForList().index(Constants.QUESTIONNAIRE + UserThreadLocal.getUserId(), 1);

        /*if (index.equals("1")) {
            redisTemplate.opsForList().set(Constants.QUESTIONNAIRE + UserThreadLocal.getUserId(), 1, "0");
        } else if (index.equals("0")) {
            redisTemplate.opsForList().set(Constants.QUESTIONNAIRE + UserThreadLocal.getUserId(), 2, "0");
        }*/

        //获取中级标识
        String intermediate = redisTemplate.opsForValue().
                get(Constants.QUESTIONNAIRE_INTERMEDIATE + UserThreadLocal.getUserId());
        Lock lock = new Lock();
        if (intermediate.equals("1")) {
            redisTemplate.opsForValue().set(Constants.QUESTIONNAIRE_INTERMEDIATE + UserThreadLocal.getUserId(), "0");
            lock.setPrimarys(0);
            lock.setIntermediate(0);
            lock.setSenior(1);
            lock.setUid(UserThreadLocal.getUserId());
            testApi.updateLock(lock);
        } else if (intermediate.equals("0")) {
            redisTemplate.opsForValue().set(Constants.QUESTIONNAIRE_SENIOR + UserThreadLocal.getUserId(), "0");
            lock.setPrimarys(0);
            lock.setIntermediate(0);
            lock.setSenior(0);
            lock.setUid(UserThreadLocal.getUserId());
            testApi.updateLock(lock);
        }
        int reportId = 0;
        //获取总分数
        int sum = 0;
        List<Long> score = new ArrayList<>();
        for (Answers answer : answers) {
            Long score1 = testApi.findScore(answer.getOptionId(), answer.getQuestionId());
            score.add(score1);
        }
        for (Long scores : score) {
            sum += scores;
        }
        if (sum <= 21) {
            reportId = 1;
        } else if (sum > 21 && sum <= 41) {
            reportId = 2;
        } else if (sum > 41 && sum <= 55) {
            reportId = 3;
        } else if (sum >= 56) {
            reportId = 4;
        }
        testApi.saveReport(UserThreadLocal.getUserId(), reportId);
        return Convert.toStr(reportId);
    }

    @Override
    public ResultVo testResult(String id) {
        Conclusion conclusion = testApi.findConclusion(id);
        ArrayList<Dimensions> dimensions = new ArrayList<>();
        dimensions.add(new Dimensions("理性", conclusion.getAbstractValue()));
        dimensions.add(new Dimensions("判断", conclusion.getJudgmentValue()));
        dimensions.add(new Dimensions("外向", conclusion.getOutValue()));
        dimensions.add(new Dimensions("抽象", conclusion.getReasonValue()));
        //根据报告id查询用户id
        List<Like> likes = testApi.findUser(id);
        List<SimilarYou> similarYous = new ArrayList<>();
        for (Like like : likes) {
            UserInfo userInfos = userInfoApi.findUserInfoByUserId(like.getId());
            SimilarYou similarYou = new SimilarYou();
            similarYou.setId(Convert.toInt(userInfos.getUserId()));
            similarYou.setAvatar(userInfos.getLogo());
            similarYous.add(similarYou);
        }
        //拼凑vo对象
        ResultVo resultVo = new ResultVo();
        resultVo.setConclusion(conclusion.getConclusion());
        resultVo.setCover(conclusion.getCover());
        resultVo.setDimensions(dimensions);
        resultVo.setSimilarYou(similarYous);
        return resultVo;
    }
}
