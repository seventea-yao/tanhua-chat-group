package com.itheima.commons.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ScoreSetion implements Serializable {
    private Long id;
    private Long scoreSetion;
    private Long couclusionId;
}
