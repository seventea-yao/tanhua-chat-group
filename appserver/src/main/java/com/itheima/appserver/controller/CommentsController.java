package com.itheima.appserver.controller;

import com.itheima.appserver.service.CommentService;
import com.itheima.commons.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("comments")
public class CommentsController {

    @Autowired
    private CommentService commentService;

    /**
     * 评论列表
     * @param publishId
     * @param page
     * @param pagesize
     * @return
     */
    @GetMapping
    public ResponseEntity queryComment(@RequestParam("movementId")String publishId,
                                    @RequestParam("page")Integer page,
                                   @RequestParam("pagesize")Integer pagesize) {
        PageResult pageResult = commentService.queryCommentsList(publishId, page, pagesize);
        return ResponseEntity.ok(pageResult);
    }

    @GetMapping("{id}/like")
    public ResponseEntity likeComment(@PathVariable("id")String publishId){
        commentService.likeComment(publishId);
        return ResponseEntity.ok(null);
    }

    @GetMapping("{id}/dislike")
    public ResponseEntity disLikeComment(@PathVariable("id")String publishId){
        commentService.disLikeComment(publishId);
        return ResponseEntity.ok(null);
    }

    @PostMapping
    public  ResponseEntity saveComment(@RequestBody Map<String,String>map){
        commentService.saveComment(map.get("movementId"),map.get("comment"));
        return ResponseEntity.ok(null);
    }
}
