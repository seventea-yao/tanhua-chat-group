package com.itheima.mysql.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.commons.pojo.UserLogin;

public interface UserLoginMapper extends BaseMapper<UserLogin> {
}
