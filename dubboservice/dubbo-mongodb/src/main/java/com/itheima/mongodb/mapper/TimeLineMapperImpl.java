package com.itheima.mongodb.mapper;

import cn.hutool.core.collection.CollUtil;
import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.commons.pojo.TimeLine;
import com.itheima.commons.pojo.Users;
import com.itheima.interfaces.TimeLineApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.scheduling.annotation.Async;

import java.util.List;

@Service
public class TimeLineMapperImpl implements TimeLineApi {

    @Autowired
    private MongoTemplate mongoTemplate;
    @Override
    public List<Long> selectTimeLine(Long userId) {
        List<Users> usersList = mongoTemplate.find(Query.query(Criteria.where("userId").is(userId)), Users.class);
        List<Long> longList = CollUtil.getFieldValues(usersList, "friendId", Long.class);

        return longList;
    }

    @Override
    @Async
    public void savePublishMessage(TimeLine timeLine, List<Long> longs) {
        for (Long aLong : longs) {
            mongoTemplate.save(timeLine,"quanzi_time_line_"+aLong);
        }
    }


}
