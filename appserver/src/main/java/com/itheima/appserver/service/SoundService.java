package com.itheima.appserver.service;

import com.itheima.commons.vo.SoundVo;
import org.springframework.web.multipart.MultipartFile;

public interface SoundService {

    void sendSound(MultipartFile multipartFile);

    SoundVo receiveSound();
}
