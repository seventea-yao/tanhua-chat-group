package com.itheima.interfaces;

import com.itheima.commons.pojo.Comment;
import com.itheima.commons.pojo.FollowUser;
import com.itheima.commons.pojo.Video;

import java.util.List;

public interface VideosApi {
    /**
     * 添加小视频
     * @param video
     * @return
     */
    Video saveVideo(Video video);

    /**
     * 查询vid
     * @param
     * @return
     */
    List<Video> selectByVid(Long[] vids);

    /**
     * 随机生成vid
     * @param pagesize
     * @return
     */
    List<Video> randomByVid(Integer pagesize);

    /**
     * 给视频点赞
     * @param comment
     */
    void likeVideo(Comment comment);

    /**
     * 通过id 查找video
     * @param publishId
     * @return
     */
    Video selectById(String publishId);

    /**
     * 根据id 删除点赞
     * @param id
     * @return
     */
    Long disLikeVideo(String id,Long userId);

    /**
     * 视频评论点赞
     *
     * @param comment
     * @return
     */
    Long commentLikeVideo(Comment comment);

    /**
     * 关注视频用户
     * @param followUser
     */
    void userFocus(FollowUser followUser);

    /**
     * 取消视频用户关注
     * @param toLong
     * @param userId
     */
    void userUnFocus(Long toLong, Long userId);

    /**
     * 视频评论
     * @param comment
     */
    void publishComment(Comment comment);

    List<Video> pageVideoLogs(Integer page, Integer pageSize, String sortProp, String sub, Long uid);
}
