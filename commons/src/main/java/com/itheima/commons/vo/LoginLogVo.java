package com.itheima.commons.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class LoginLogVo implements Serializable {

    private Long id;
    private Long logTime;
    private String place;
    private String equipment;

}