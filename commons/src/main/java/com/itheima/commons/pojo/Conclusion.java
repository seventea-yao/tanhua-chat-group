package com.itheima.commons.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Conclusion implements Serializable {
    private Long id;
    private String conclusion;
    private String cover;
    private String outValue;
    private String reasonValue;
    private String abstractValue;
    private String judgmentValue;

}
