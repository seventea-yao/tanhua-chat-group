package com.itheima.manage.interceptor;

import com.itheima.commons.pojo.ManageUser;
import com.itheima.commons.pojo.User;

public class UserThreadLocal {
    public static final ThreadLocal<ManageUser> THREAD_LOCAL_USER = new ThreadLocal<>();


    public static void setUser(ManageUser user){
        THREAD_LOCAL_USER.set(user);
    }

    public static Long getUserId(){
        return THREAD_LOCAL_USER.get().getId();
    }


    public static String getUsername(){
        return THREAD_LOCAL_USER.get().getUsername();
    }

    public static void remover(){
        THREAD_LOCAL_USER.remove();
    }
}
