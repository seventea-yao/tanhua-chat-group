package com.itheima.mysql.impl;
import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.itheima.commons.pojo.ManageUser;
import com.itheima.interfaces.ManageUserApi;
import com.itheima.mysql.mapper.ManageUserMapper;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class ManageUserImpl implements ManageUserApi {

    @Autowired
    ManageUserMapper manageUserMapper;

    @Override
    public ManageUser login(String username, String password) {
        return manageUserMapper.selectOne(Wrappers.lambdaQuery(ManageUser.class)
                .eq(ManageUser::getUsername, username)
                .eq(ManageUser::getPassword, password));
    }
}
