package com.itheima.mongodb.mapper;

import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.commons.pojo.Comment;
import com.itheima.commons.pojo.FollowUser;
import com.itheima.commons.pojo.Video;
import com.itheima.interfaces.VideosApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

@Service
public class VideoMapperImpl implements VideosApi {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public Video saveVideo(Video video) {

        return mongoTemplate.save(video);
    }

    @Override
    public List<Video> selectByVid(Long[] vids) {
        List<Video> videoList = mongoTemplate.find(Query.query(Criteria.where("vid").in(vids)), Video.class);
        return videoList;
    }

    @Override
    public List<Video> randomByVid(Integer pagesize) {
        AggregationResults<Video> aggregate = mongoTemplate.aggregate(Aggregation.newAggregation(Video.class, Aggregation.sample(pagesize)), Video.class);
        return aggregate.getMappedResults();
    }

    @Override
    public void likeVideo(Comment comment) {
        mongoTemplate.save(comment);
    }

    @Override
    public Video selectById(String publishId) {
        return mongoTemplate.findOne(Query.query(Criteria.where("id").is(publishId)), Video.class);
    }

    @Override
    public Long disLikeVideo(String id, Long userId) {
        mongoTemplate.remove(Query.query(Criteria.where("id").is(id).and("commentType").is(1).and("userId").is(userId)), Comment.class);
        long count = mongoTemplate.count(Query.query(Criteria.where("id").is(id).and("commentType").is(1)), Comment.class);
        return count;
    }

    @Override
    public Long commentLikeVideo(Comment comment) {
        mongoTemplate.save(comment);
        long count = mongoTemplate.count(Query.query(Criteria.where("publishId").is(comment.getId()).and("commentType").is(1)), Comment.class);
        return count;
    }

    @Override
    public void userFocus(FollowUser followUser) {
        mongoTemplate.save(followUser);
    }

    @Override
    public void userUnFocus(Long toLong, Long userId) {
        mongoTemplate.remove(Query.query(Criteria.where("follwoUserId").is(toLong).and("userId").is(userId)), FollowUser.class);
    }

    @Override
    public void publishComment(Comment comment) {
        mongoTemplate.save(comment);
    }

    @Override
    public List<Video> pageVideoLogs(Integer page, Integer pageSize, String sortProp, String sub, Long uid) {
        Query query = new Query();
        query.addCriteria(Criteria.where("userId").is(uid));
        query.with(PageRequest.of(page - 1, pageSize));

        if ("desc".equals(sub)) {
            query.with(Sort.by(Sort.Order.desc("created")));
        } else if ("asc".equals(sub)) {
            query.with(Sort.by(Sort.Order.asc("created")));
        }
        return mongoTemplate.find(query, Video.class);

    }

}
