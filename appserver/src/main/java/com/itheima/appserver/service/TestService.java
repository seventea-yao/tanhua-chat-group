package com.itheima.appserver.service;


import com.itheima.commons.pojo.Answers;
import com.itheima.commons.vo.QuestionnaireVo;
import com.itheima.commons.vo.ResultVo;

import java.util.List;

public interface TestService {
    List<QuestionnaireVo> sendSound();

   String testSubmit(List<Answers> answers);


    ResultVo testResult(String id);
}
