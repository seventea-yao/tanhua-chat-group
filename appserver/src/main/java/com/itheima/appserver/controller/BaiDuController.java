package com.itheima.appserver.controller;

import com.itheima.appserver.service.BaiDuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("baidu")
public class BaiDuController {

    @Autowired
    private BaiDuService baiDuService;

    /**
     * 上传自己位置
     * @param @RequestBody Map<String,Object>map
     * @return
     */
    @PostMapping("location")
    public ResponseEntity location(@RequestParam("latitude") Double latitude,
                                   @RequestParam("longitude") Double longitude,
                                   @RequestParam("addrStr")String addrStr) {
       /* Double latitude = Convert.toDouble(map.get("latitude"));
        Double longitude = Convert.toDouble(map.get("longitude"));
        String addrStr = Convert.toStr(map.get("addrStr"));*/
        baiDuService.location(latitude,longitude,addrStr);
        return ResponseEntity.ok(null);
    }
}
