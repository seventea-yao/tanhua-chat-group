package com.itheima.mysql.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.commons.pojo.Announcement;

public interface AnnouncementMapper extends BaseMapper<Announcement> {
}
