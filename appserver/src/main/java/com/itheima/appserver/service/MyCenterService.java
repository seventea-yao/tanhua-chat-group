package com.itheima.appserver.service;

import com.itheima.commons.vo.CountsVo;
import com.itheima.commons.vo.PageResult;
import com.itheima.commons.vo.SettingsVo;
import com.itheima.commons.vo.UserInfoVo;
import org.springframework.web.multipart.MultipartFile;

public interface MyCenterService {
    UserInfoVo queryUserInfoByUserId(Long userID);

    CountsVo counts();

    PageResult queryLikeList(Integer type, Integer page, Integer pagesize, String nickname);

    void cancelLove(Long uid);

    void fansLove(Long uid);

    void putUserInfo(String nickname, String birthday);

    void updateHeader(MultipartFile headPhoto);

    SettingsVo getSettings();

    void settingQuestion(String content);

    PageResult blacklist(Integer page, Integer pagesize);

    void removeBlackList(Long uid);

    void notifications(Boolean likeNotification, Boolean pinglunNotification, Boolean gonggaoNotification);

    void sendVerificationCode();

    Boolean checkVerificationCode(String verificationCode);

    void savePhone(String phone);
}
