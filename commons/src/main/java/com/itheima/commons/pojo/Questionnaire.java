package com.itheima.commons.pojo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Questionnaire implements Serializable {
    private String id;
    private String name;
    private Integer star;
    private String cover;
    private Integer isLock;
    private String level;

}
