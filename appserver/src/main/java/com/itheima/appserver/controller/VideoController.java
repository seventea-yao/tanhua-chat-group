package com.itheima.appserver.controller;

import com.itheima.appserver.service.VideoService;
import com.itheima.commons.vo.CommentVo;
import com.itheima.commons.vo.PageResult;
import com.itheima.commons.vo.VideoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

@RestController
@RequestMapping("smallVideos")
public class VideoController {

    @Autowired
    private VideoService videoService;

    /**
     * 上传视频
     * @param videoThumbnail
     * @param videoFile
     * @return
     */
    @PostMapping
    public ResponseEntity loadVideo(@RequestParam("videoThumbnail") MultipartFile videoThumbnail,
                                    @RequestParam("videoFile") MultipartFile videoFile){
        videoService.loadVideo(videoThumbnail,videoFile);
        return ResponseEntity.ok(null);
    }

    /**
     * 小视频列表
     * @param page
     * @param pagesize
     * @return
     */
    @GetMapping
    public ResponseEntity videoList(@RequestParam(value = "page",defaultValue = "1") Integer page, @RequestParam(value = "pagesize",defaultValue = "10") Integer pagesize) {
        PageResult pageResult = videoService.videoList(page, pagesize);
        return ResponseEntity.ok(pageResult);
    }

    /**
     * 视频点赞
     * @param id
     * @return
     */
    @PostMapping("{id}/like")
    public ResponseEntity videoLike(@PathVariable("id")String id){
        videoService.videoLike(id);
        return ResponseEntity.ok(null);
    }

    /**
     * 取消视频点赞
     * @return
     */
    @PostMapping("{id}/dislike")
    public ResponseEntity videoDisLIke(@PathVariable("id")String id){
        videoService.videoDisLike(id);
        return ResponseEntity.ok(null);
    }

    /**
     * 视频评论
     * @param id
     * @param context
     * @return
     */
    @PostMapping("{id}/comments")
    public ResponseEntity publishComments(@PathVariable("id")String id, @RequestBody Map<String,String> context){
        videoService.publishComment(id,context.get("comment"));
        return ResponseEntity.ok(null);
    }



    /**
     * 评论列表
     */
    @GetMapping("{id}/comments")
    public ResponseEntity queryCommentsList(@PathVariable("id")String movementId,
                                            @RequestParam("page")Integer page,
                                            @RequestParam("pagesize")Integer pagesize){
        PageResult pageResult = videoService.queryCommentsList(movementId, page, pagesize);
        return  ResponseEntity.ok(pageResult);
    }

    /**
     * 视频评论点赞
     * @param id
     * @return
     */
    @PostMapping("comments/{id}/like")
    public ResponseEntity videoCommentLike(@PathVariable("id")String id){
        videoService.videoCommentLike(id);
        return ResponseEntity.ok(null);
    }

    /**
     * 取消视频评论点赞
     * @param id
     * @return
     */
    @PostMapping("comments/{id}/dislike")
    public ResponseEntity videoCommentDisLike(@PathVariable("id")String id) {
        videoService.videoCommentDisLike(id);
        return ResponseEntity.ok(null);
    }

    /**
     * 关注视频用户
     * @param id
     * @return
     */
    @PostMapping("{uid}/userFocus")
    public ResponseEntity userFocus(@PathVariable("uid")String id){
        videoService.userFocus(id);
        return ResponseEntity.ok(null);
    }

    /**
     * 取消视频用户关注
     * @param id
     * @return
     */
    @PostMapping("{uid}/userUnFocus")
    public ResponseEntity userUnFocus(@PathVariable("uid")String id){
        videoService.userUnFocus(id);
        return ResponseEntity.ok(null);
    }

}
