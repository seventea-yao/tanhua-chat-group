package com.itheima.mysql.impl;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.commons.pojo.AuditState;
import com.itheima.interfaces.AuditStateApi;
import com.itheima.mysql.mapper.AuditStateMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service
public class AuditStateApiImpl implements AuditStateApi {
    @Autowired
    private AuditStateMapper auditStateMapper;

    @Override
    public void addAuditState(AuditState auditState) {
        auditStateMapper.insert(auditState);
    }

    @Override
    public AuditState queryAuditState(String publishId) {
        return auditStateMapper.selectOne(Wrappers.lambdaQuery(AuditState.class).eq(AuditState::getPublishId, publishId));
    }

    @Override
    public Page<AuditState> queryAuditStatesByStateAndPage(Integer page, Integer pageSize, String state, Long id, Long sd, Long ed) {

        return auditStateMapper.selectPage(new Page<>(page - 1, pageSize), Wrappers.lambdaQuery(AuditState.class)
                        .eq(!state.equals("all"), AuditState::getState, state)
                        .eq(ObjectUtil.isNotNull(id), AuditState::getUserId, id)
                        .gt(sd != -1, AuditState::getCreated, new DateTime(sd).toString())
                        .le(ed != -1, AuditState::getCreated, new DateTime(ed).toString())
        );
    }

    @Override
    public Long findNumByState(String state,Long id,Long sd, Long ed) {
        Integer integer = auditStateMapper.selectCount(Wrappers.lambdaQuery(AuditState.class)
                .eq(ObjectUtil.isNotNull(state), AuditState::getState, state)
                .eq(ObjectUtil.isNotNull(id), AuditState::getUserId, id)
                .gt(sd != -1, AuditState::getCreated, new DateTime(sd).toString())
                .le(ed != -1, AuditState::getCreated, new DateTime(ed).toString()));
        return Convert.toLong(integer);
    }

    @Override
    public Boolean setAuditStateTopState(String id, Integer topState) {

       /* UpdateWrapper<AuditState> wrapper = new UpdateWrapper<>();
        wrapper.set("topState", topState).eq("publishId", id);

        return 1<= auditStateMapper.update(new AuditState(), wrapper);*/

        return 1 <= auditStateMapper.update(AuditState.builder()
                .topState(topState).build(), Wrappers.lambdaQuery(AuditState.class).eq(AuditState::getPublishId, id));
    }

    @Override
    public List<AuditState> selectAuditState(String i) {
        return auditStateMapper.selectList(Wrappers.lambdaQuery(AuditState.class).eq(AuditState::getState, i));
    }

    @Override
    public void updateState(String publishId, int i) {
        auditStateMapper.update(AuditState.builder().state(i + "").build(), Wrappers.lambdaQuery(AuditState.class).eq(AuditState::getPublishId, publishId));
    }

    @Override
    public void saveState(AuditState auditState) {
        auditStateMapper.insert(auditState);
    }

    @Override
    public Boolean updateAuditState(String[] publishIds, String state) {

        /*UpdateWrapper<AuditState> wrapper = new UpdateWrapper<>();
        wrapper.set("state", state).in("publishId", publishIds);

        return 1 <= auditStateMapper.update(new AuditState(), wrapper);
*/
        return 1 <= auditStateMapper.update(AuditState.builder()
                .state(state).build(), Wrappers.lambdaQuery(AuditState.class).in(AuditState::getPublishId, publishIds));

    }

}
