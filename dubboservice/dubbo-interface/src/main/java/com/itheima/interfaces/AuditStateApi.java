package com.itheima.interfaces;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.commons.pojo.AuditState;
import org.bson.types.ObjectId;

import java.util.List;


public interface AuditStateApi {
    /**
     * 添加 AuditState
     * @param auditState
     */
    void addAuditState(AuditState auditState);

    /**
     * 修改 依据publishId
     * @param publishIds 被操作的动态Id数组
     * @param state 要修改为什么状态
     */
    Boolean updateAuditState(String[] publishIds,String state);

    /**
     * 根据publishId 查动态审核状态
     * @param publishId
     * @return
     */
    AuditState queryAuditState(String publishId);


    /**
     * 分页查询 根据状态查 返回Page<AuditState>
     * @param page
     * @param pageSize
     * @param state
     * @return
     */
    Page<AuditState> queryAuditStatesByStateAndPage(Integer page, Integer pageSize,String state,Long id,Long sd,Long ed);

    /**
     * 查询数量 根据状态state
     * @param state
     * @return
     */
    Long findNumByState(String state,Long id,Long sd, Long ed);

    /**
     * 消息置顶状态更新
     * @param id 被操作的动态Id
     * @param topState 要更新为的状态
     * @return
     */
    Boolean setAuditStateTopState(String id,Integer topState);

    List<AuditState> selectAuditState(String s);

    void updateState(String publishId, int i);

    void saveState(AuditState auditState);

}
