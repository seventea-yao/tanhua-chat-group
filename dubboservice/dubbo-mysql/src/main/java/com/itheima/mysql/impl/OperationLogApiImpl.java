package com.itheima.mysql.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.itheima.commons.vo.OperationLog;
import com.itheima.interfaces.OperationLogApi;
import com.itheima.mysql.mapper.OperationLogMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service
public class OperationLogApiImpl implements OperationLogApi {

    @Autowired
    private OperationLogMapper operationLogMapper;

    @Override
    public List<OperationLog> selectOperation(String st, String et, String name, String text) {
        return operationLogMapper.selectList(Wrappers.lambdaQuery(OperationLog.class).eq(OperationLog::getOperationText, text).between(OperationLog::getOperationTime, st, et));
    }
}
