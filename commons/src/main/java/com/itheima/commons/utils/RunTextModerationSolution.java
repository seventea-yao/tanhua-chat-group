package com.itheima.commons.utils;

import com.alibaba.fastjson.JSON;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.moderation.v2.region.ModerationRegion;
import com.huaweicloud.sdk.moderation.v2.*;
import com.huaweicloud.sdk.moderation.v2.model.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RunTextModerationSolution implements Serializable {

    public static String textCheck(String text) {
        String ak = "XRCIBQ6VWFF0HH3X53RU";
        String sk = "HYcGT8HsdidLAtW2uscCzRXB0r1Fb2Kg2juWp7fZ";

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        ModerationClient client = ModerationClient.newBuilder()
                .withCredential(auth)
                .withRegion(ModerationRegion.valueOf("cn-north-4"))
                .build();
        RunTextModerationRequest request = new RunTextModerationRequest();
        TextDetectionReq body = new TextDetectionReq();
        List<TextDetectionItemsReq> listbodyItems = new ArrayList<>();
        listbodyItems.add(
                new TextDetectionItemsReq()
                        .withText(text)
                        .withType("content")
        );
        List<String> listbodyCategories = new ArrayList<>();
        listbodyCategories.add("ad");
        listbodyCategories.add("politics");
        listbodyCategories.add("abuse");
        listbodyCategories.add("porn");
        listbodyCategories.add("contraband");
        listbodyCategories.add("flood");
        body.withItems(listbodyItems);
        body.withCategories(listbodyCategories);
        request.withBody(body);
        try {
            RunTextModerationResponse response = client.runTextModeration(request);
            System.out.println(response.getHttpStatusCode());
            System.out.println(JSON.toJSON(response));
            return response.getResult().getSuggestion();
        } catch (ConnectionException e) {
            e.printStackTrace();
        } catch (RequestTimeoutException e) {
            e.printStackTrace();
        } catch (ServiceResponseException e) {
            e.printStackTrace();
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
        return null;
    }


    public static String imgCheck(String imgPath) {
        String ak = "XRCIBQ6VWFF0HH3X53RU";
        String sk = "HYcGT8HsdidLAtW2uscCzRXB0r1Fb2Kg2juWp7fZ";
        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);
        ModerationClient client = ModerationClient.newBuilder()
                .withCredential(auth)
                .withRegion(ModerationRegion.valueOf("cn-north-4"))
                .build();
        RunImageModerationRequest request = new RunImageModerationRequest();
        ImageDetectionReq body = new ImageDetectionReq();
        List<ImageDetectionReq.CategoriesEnum> listbodyCategories = new ArrayList<>();
        listbodyCategories.add(ImageDetectionReq.CategoriesEnum.fromValue("politics"));
        listbodyCategories.add(ImageDetectionReq.CategoriesEnum.fromValue("terrorism"));
        listbodyCategories.add(ImageDetectionReq.CategoriesEnum.fromValue("porn"));
        body.withCategories(listbodyCategories);
        body.withModerationRule("default");
        body.withUrl(imgPath);
        request.withBody(body);
        try {
            RunImageModerationResponse response = client.runImageModeration(request);
            System.out.println(response.getHttpStatusCode());
            System.out.println(JSON.toJSONString(response));
            return response.getResult().getSuggestion();
        } catch (ConnectionException e) {
            e.printStackTrace();
        } catch (RequestTimeoutException e) {
            e.printStackTrace();
        } catch (ServiceResponseException e) {
            e.printStackTrace();
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
        return null;
    }

    public static Integer imgCheck(List<String> imgPath) {
        String ak = "XRCIBQ6VWFF0HH3X53RU";
        String sk = "HYcGT8HsdidLAtW2uscCzRXB0r1Fb2Kg2juWp7fZ";
        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);
        ModerationClient client = ModerationClient.newBuilder()
                .withCredential(auth)
                .withRegion(ModerationRegion.valueOf("cn-north-4"))
                .build();
        RunImageBatchModerationRequest request = new RunImageBatchModerationRequest();
        ImageBatchModerationReq body = new ImageBatchModerationReq();
        List<ImageBatchModerationReq.CategoriesEnum> listbodyCategories = new ArrayList<>();
        listbodyCategories.add(ImageBatchModerationReq.CategoriesEnum.fromValue("politics"));
        listbodyCategories.add(ImageBatchModerationReq.CategoriesEnum.fromValue("terrorism"));
        listbodyCategories.add(ImageBatchModerationReq.CategoriesEnum.fromValue("porn"));
        List<String> listbodyUrls = new ArrayList<>();
        for (String s : imgPath) {
            listbodyUrls.add(s);
        }
        body.withThreshold((double) 0);
        body.withCategories(listbodyCategories);
        body.withUrls(listbodyUrls);
        request.withBody(body);
        try {
            RunImageBatchModerationResponse response = client.runImageBatchModeration(request);
            System.out.println(response.getHttpStatusCode());
            System.out.println(JSON.toJSONString(response));
            return response.getHttpStatusCode();
        } catch (ConnectionException e) {
            e.printStackTrace();
        } catch (RequestTimeoutException e) {
            e.printStackTrace();
        } catch (ServiceResponseException e) {
            e.printStackTrace();
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
        return null;
    }

    public static void main(String[] args) {
        String ak = "XRCIBQ6VWFF0HH3X53RU";
        String sk = "HYcGT8HsdidLAtW2uscCzRXB0r1Fb2Kg2juWp7fZ";

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        ModerationClient client = ModerationClient.newBuilder()
                .withCredential(auth)
                .withRegion(ModerationRegion.valueOf("cn-north-4"))
                .build();
        RunTextModerationRequest request = new RunTextModerationRequest();
        TextDetectionReq body = new TextDetectionReq();
        List<TextDetectionItemsReq> listbodyItems = new ArrayList<>();
        listbodyItems.add(
                new TextDetectionItemsReq()
                        .withText("西瓜鱼商城是一款专门为客户定制的应用，它可以让用户购买各种商品。当用户购买需要上门\n" +
                                "安装的电器时，用户能够及时的享受到上门服务\n" +
                                "职责描述:\n" +
                                "1、协助组内其他成员开发\n" +
                                "2、完善了该项目的相关文档资料\n" +
                                "3、收集封装数据，采用 JavaScript、Validator 验证框架对表单数据进行合法性校验")
                        .withType("content")
        );
        List<String> listbodyCategories = new ArrayList<>();
        listbodyCategories.add("ad");
        listbodyCategories.add("politics");
        listbodyCategories.add("abuse");
        listbodyCategories.add("porn");
        listbodyCategories.add("contraband");
        listbodyCategories.add("flood");
        body.withItems(listbodyItems);
        body.withCategories(listbodyCategories);
        request.withBody(body);
        try {
            RunTextModerationResponse response = client.runTextModeration(request);
            System.out.println(response.getHttpStatusCode());
            System.out.println(JSON.toJSON(response));
        } catch (ConnectionException e) {
            e.printStackTrace();
        } catch (RequestTimeoutException e) {
            e.printStackTrace();
        } catch (ServiceResponseException e) {
            e.printStackTrace();
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }
}