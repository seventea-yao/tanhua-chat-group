package com.itheima.interfaces;

import com.itheima.commons.pojo.Sound;


public interface SoundApi {
    void saveSound(Sound sound);

    Sound receiveSound();
}
