package com.itheima.manage.controller;

import com.itheima.commons.vo.OperationLog;
import com.itheima.commons.vo.PostLoginDayVo;
import com.itheima.manage.interceptor.NoAuthorization;
import com.itheima.manage.service.PostManService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("user")
public class PostManController {

    @Autowired
    private PostManService postManService;


    @GetMapping("preserve")
    @NoAuthorization
    public ResponseEntity loginDay(@RequestParam(value = "st", required = false) String st,
                                   @RequestParam(value = "et", required = false) String et,
                                   @RequestParam(value = "sex", required = false) Integer sex) {
        List<PostLoginDayVo> postLoginDayVo = postManService.selectRegisterNum(st, et, sex);
        return ResponseEntity.ok(postLoginDayVo);
    }

    @GetMapping("operation")
    @NoAuthorization
    public ResponseEntity operation(@RequestParam(value = "st", required = false) String st,
                                    @RequestParam(value = "et", required = false) String et,
                                    @RequestParam(value = "name", required = false) String name,
                                    @RequestParam(value = "text", required = false) String text) {
        List<OperationLog> operationLogs = postManService.selectOperationLog(st, et, name, text);
        return ResponseEntity.ok(operationLogs);
    }


}
