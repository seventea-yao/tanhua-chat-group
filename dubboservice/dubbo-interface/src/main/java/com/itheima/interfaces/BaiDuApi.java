package com.itheima.interfaces;

import com.itheima.commons.vo.UserLocationVo;

import java.util.List;

public interface BaiDuApi {
    /**
     * 上传地理位置
     * @param latitude
     * @param longitude
     * @param addrStr
     * @return
     */
    Boolean saveLocation(Double latitude, Double longitude, String addrStr,Long userId);

    /**
     * 查询自己的地理位置
     * @param userId
     * @return
     */
    UserLocationVo selectByUserId(Long userId);

    /**
     * 搜附近
     * @param latitude
     * @param longitude
     * @param distance
     * @return
     */
    List<UserLocationVo> selectSearch(Double latitude, Double longitude, Integer counts, Double distance);
}
