package com.itheima.mysql.impl;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.util.StrUtil;
import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.commons.pojo.User;
import com.itheima.commons.pojo.UserLogin;
import com.itheima.interfaces.UserLoginApi;
import com.itheima.mysql.mapper.UserLoginMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

@Service
public class UserLoginMapperImpl implements UserLoginApi {
    @Autowired
    UserLoginMapper userLoginMapper;


    @Override
    public void addUserLogin(UserLogin userLogin) {
        userLoginMapper.insert(userLogin);
    }

    @Override
    public Integer findActiveUser(Date start, Date end) {
        QueryWrapper<UserLogin> queryWrapper = new QueryWrapper<>();
        return userLoginMapper.selectCount(queryWrapper.select("DISTINCT user_id")
                .between("login_time", start, end)
        );
    }

    @Override
    public Integer loginUserCount(Date start, Date end) {
        return userLoginMapper.selectCount(Wrappers.lambdaQuery(UserLogin.class)
                .between(UserLogin::getLoginTime, start, end));
    }

    @Override
    public List<UserLogin> findUser(Date start, Date end) {
        return userLoginMapper.selectList(Wrappers.lambdaQuery(UserLogin.class)
                .between(UserLogin::getLoginTime, start, end)
        );
    }

    @Override
    public Integer selectUserLogin(List<Long> values, DateTime oneDay1, DateTime oneDay2) {
        return userLoginMapper.selectCount(Wrappers.lambdaQuery(UserLogin.class).in(UserLogin::getUserId, values).between(UserLogin::getLoginTime, oneDay1, oneDay2));
    }

    @Override
    public Page<UserLogin> pageLoginLogs(Integer page, Integer pageSize, String sub, Long uid) {
        return userLoginMapper.selectPage(new Page<>(page, pageSize),
                Wrappers.lambdaQuery(UserLogin.class)
                        .eq(UserLogin::getUserId, uid)
                        .orderBy(StrUtil.isNotBlank(sub) && "asc".equalsIgnoreCase(sub), true, UserLogin::getLoginTime)
                        .orderBy(StrUtil.isNotBlank(sub) && "desc".equalsIgnoreCase(sub), false, UserLogin::getLoginTime));
    }

    @Override
    public List<UserLogin> findLoginCount(Date startDate, Date endDate, List<Long> ids) {
        return userLoginMapper.selectList(Wrappers.lambdaQuery(UserLogin.class).in(UserLogin::getUserId, ids).between(UserLogin::getLoginTime, startDate, endDate).orderByAsc(UserLogin::getLoginTime));
    }

}