package com.itheima.appserver.service;

import com.itheima.commons.vo.PageResult;
import com.itheima.commons.vo.QuanZiVo;
import com.itheima.commons.vo.VisitorsVo;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface PublishService {
    void publishMessage(String textContent, String location, String longitude, String latitude, MultipartFile[] multipartFile);

    PageResult findFriendMovements(Integer page, Integer pagesize);


    PageResult findRecommendFriend(Integer page, Integer pagesize);


    Long likeComment(String id);

    Long noLikeComment(String id);

    Long loveComment(String id);

    Long noLoveComment(String id);

    QuanZiVo queryById(String id);

    PageResult queryAlbumList(Integer page, Integer pagesize, Long userId);

    List<VisitorsVo> visitors();

}
