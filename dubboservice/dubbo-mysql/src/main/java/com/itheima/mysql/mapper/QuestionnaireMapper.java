package com.itheima.mysql.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.commons.pojo.Questionnaire;

public interface QuestionnaireMapper extends BaseMapper<Questionnaire> {
}
