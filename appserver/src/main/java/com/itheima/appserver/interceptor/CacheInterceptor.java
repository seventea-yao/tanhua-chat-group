/*
package com.itheima.appserver.interceptor;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.json.JSON;
import cn.hutool.json.JSONUtil;
import com.itheima.appserver.annotation.MyCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@Component
public class CacheInterceptor implements HandlerInterceptor {

    @Autowired
    private  RedisTemplate<String,String> redisTemplate;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }
        if (!((HandlerMethod) handler).hasMethodAnnotation(GetMapping.class)) {
            return true;
        }
        if (!((HandlerMethod) handler).hasMethodAnnotation(MyCache.class)) {
            return true;
        }

        String redisKey = redisTemplate.opsForValue().get(redisKey(request));
        if (StrUtil.isBlank(redisKey)) {
            return true;
        }
        response.setContentType("application/json;charset=utf-8");
        response.getWriter().write(redisKey);

        return false;
    }

    public static  String redisKey(HttpServletRequest request) {
        String uri = request.getRequestURI();
        Map<String, String[]> parameterMap = request.getParameterMap();
        String token = request.getHeader("Authorization");
        String jsonStr = JSONUtil.toJsonStr(parameterMap);
        String md5 = SecureUtil.md5(uri + "_" + jsonStr + "_" + token);
        return md5;

    }



}
*/
