package com.itheima.interfaces;


import cn.hutool.core.date.DateTime;
import com.itheima.commons.pojo.User;

import java.util.Date;
import java.util.List;

public interface UserApi {

    /**
     * 根据手机号查询
     *
     * @param phone
     * @return
     */
    User selectUserByPhone(String phone);

    User addUser(User user);

    /**
     * 查询用户手机号
     *
     * @param userId
     * @return
     */
    User selectPhoneById(Long userId);

    /**
     * 修改手机号
     *
     * @param userId
     * @param phone
     */
    void savePhone(Long userId, String phone);

    List<User> selectAll();

    /**
     * 查询用户数量
     *
     * @return
     */
    Integer selectUserCount();

    /**
     * 查询今日新增用户数量
     *
     * @param pastDate
     * @return
     */
    Integer selectNewUser(String pastDate);

    Integer selectNewUserCount(String startDate, String endDate);

    int allCount();


    int registerUser(Date start, Date end);

    List<User> registerUserList(Date startDate, Date endDate);

    List<User> selectNowDayUser(String dateTime, String endOfDay,String sex);
}
