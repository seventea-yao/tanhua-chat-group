package com.itheima.commons.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class BlackList implements Serializable {
    private Long id;
    private Long userId;
    private Long blackUserId;
    private Date created;
    private Date updated;
}
