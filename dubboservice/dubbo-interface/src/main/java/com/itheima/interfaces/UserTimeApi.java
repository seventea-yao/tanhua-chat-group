package com.itheima.interfaces;


import cn.hutool.core.date.DateTime;

import java.util.Date;
import java.util.List;

public interface UserTimeApi {
    /**
     * 查询过去几天活跃人数
     *
     * @param startTime
     * @param endTime
     * @return
     */
    Integer selectActiveUser(String startTime, String endTime);

    /**
     * 查询一天活跃用户
     * @param today
     * @return
     */
    Integer selectActiveUser(String today);

    /**
     * 添加用户登录时间
     * @param id
     * @param loginTime
     */
    void saveLoginTime(Long id, Date loginTime);

    /**
     * 今日登录次数
     * @param pastDate
     * @return
     */
    Integer selectLoginToday(String pastDate);

    /**
     * 查询活跃人数(月)
     * @param startDate
     * @param endDate
     * @return
     */
    Integer selectNewUserCount(String startDate, String endDate);

    Integer selectUserLogin(List<Long> values, String oneDay1, String oneDay2);
}
