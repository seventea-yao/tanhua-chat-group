package com.itheima.mysql.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.commons.pojo.Settings;

public interface SettingsMapper  extends BaseMapper<Settings> {
}
