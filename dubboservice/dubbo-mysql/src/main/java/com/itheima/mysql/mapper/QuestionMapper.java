package com.itheima.mysql.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.commons.pojo.Question;

public interface QuestionMapper extends BaseMapper<Question> {
}
