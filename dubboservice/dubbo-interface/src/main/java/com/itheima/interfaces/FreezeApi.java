package com.itheima.interfaces;

import com.itheima.commons.pojo.Freeze;

public interface FreezeApi {

    /**
     * 添加一条冻结信息
     * @param freeze
     * @return
     */
    Boolean addFreeze(Freeze freeze);

    /**
     * 用户解冻操作
     * @param userId
     * @return
     */
    Boolean deleteFreezeByUserId(Integer userId);

    /**
     * 查询Freeze
     * @param userIdL 被查用户Id
     * @return
     */
    Freeze quweyFreezeByUserId(Long userIdL);

}
