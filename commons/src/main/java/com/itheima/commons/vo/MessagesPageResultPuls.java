package com.itheima.commons.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
//消息翻页Vo
public class MessagesPageResultPuls implements Serializable {

        private Long counts = 0L;//总记录数
    private Integer pagesize=10;//页大小
    private Long pages = 0L;//总页数
    private Integer page=1;//当前页码
    private List<MessagesVo> items = Collections.emptyList(); //列表
    private List<MessagesStateVo> totals = Collections.emptyList(); //状态合集

    public MessagesPageResultPuls(Integer page, Integer pagesize,
                          Long counts, List<MessagesVo> items,List<MessagesStateVo> totals) {
        this.page = page;
        this.pagesize = pagesize;
        this.items = items;
        this.totals = totals;
        this.counts = counts;
        this.pages =  counts % pagesize == 0 ? counts / pagesize : counts / pagesize + 1;
    }
}
