package com.itheima.mysql.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.commons.pojo.BlackList;
import com.itheima.interfaces.BlacklistApi;
import com.itheima.mysql.mapper.BlacklistMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service
public class BlacklistMapperImpl implements BlacklistApi {
    @Autowired
    private BlacklistMapper blacklistMapper;

    @Override
    public List<BlackList> selectBlackList(Long userId, Integer page, Integer pagesize) {

        Page<BlackList> page1 = new Page<>(page-1, pagesize);
        Page<BlackList> blackListPage = blacklistMapper.selectPage(page1,Wrappers.lambdaQuery(BlackList.class).eq(BlackList::getUserId,userId));
        return blackListPage.getRecords();
    }

    @Override
    public void deleteBlackList(Long userId, Long uid) {
        blacklistMapper.delete(Wrappers.lambdaQuery(BlackList.class).eq(BlackList::getUserId,userId).eq(BlackList::getBlackUserId,uid));
    }
}
