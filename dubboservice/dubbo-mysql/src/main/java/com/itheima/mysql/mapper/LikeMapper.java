package com.itheima.mysql.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.commons.pojo.Like;

public interface LikeMapper extends BaseMapper<Like> {
}
