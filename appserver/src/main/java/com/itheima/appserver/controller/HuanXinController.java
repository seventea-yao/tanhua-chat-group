package com.itheima.appserver.controller;

import com.itheima.appserver.service.HuanXinService;
import com.itheima.commons.pojo.HuanxinUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RequestMapping("huanxin")
@RestController
public class HuanXinController {

    @Autowired
    private HuanXinService huanXinService;

    @GetMapping("user")
    public Map userInfo() {
        HashMap<String, String> map = new HashMap<>();
        HuanxinUser huanxinUser = huanXinService.findHuanXinUser();
        map.put("username",huanxinUser.getUsername());
        map.put("password",huanxinUser.getPassword());
        return map;
    }
}
