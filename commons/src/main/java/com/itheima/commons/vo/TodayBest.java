package com.itheima.commons.vo;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import com.itheima.commons.pojo.RecommendUser;
import com.itheima.commons.pojo.UserInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TodayBest {

    private Long id; //用户id
    private String avatar;
    private String nickname;
    private String gender; //性别 man woman
    private Integer age;
    private String[] tags;
    private Long fateValue; //缘分值

    /**
     * 在vo对象中，补充一个工具方法，封装转化过程
     */
    public static TodayBest init(UserInfo userInfo,RecommendUser recommendUser){
        if (ObjectUtil.isEmpty(userInfo)) {
            return null;
        }
        TodayBest todayBest = new TodayBest();
        todayBest.setId(userInfo.getUserId());
        todayBest.setAvatar(userInfo.getLogo());
        todayBest.setNickname(userInfo.getNickName());
        todayBest.setGender(userInfo.getSex()==1 ? "man" : "woman");
        todayBest.setAge(userInfo.getAge());
        if (userInfo.getTags() != null){
            todayBest.setTags(userInfo.getTags().split(","));
        }
        todayBest.setFateValue(Convert.toLong(recommendUser.getScore()));
        return todayBest;
    }
}