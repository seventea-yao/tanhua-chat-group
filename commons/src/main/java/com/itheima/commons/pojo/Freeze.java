package com.itheima.commons.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
//用户冻结
public class Freeze implements Serializable {
    private long id;
    private Long userId;//用户id
    private Integer freezingTime;//冻结时间，1为冻结3天，2为冻结7天，3为永久冻结
    private Integer freezingRange;//冻结范围，1为冻结登录，2为冻结发言，3为冻结发布动态
    private String reasonsForFreezing;//冻结原因
    private String frozenRemarks;//冻结备注
    private Integer state;
}
