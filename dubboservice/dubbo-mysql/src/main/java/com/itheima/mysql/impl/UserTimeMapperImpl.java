package com.itheima.mysql.impl;

import cn.hutool.core.date.DateTime;
import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.itheima.commons.pojo.UserLogin;
import com.itheima.commons.pojo.UserTime;
import com.itheima.interfaces.UserTimeApi;
import com.itheima.mysql.mapper.UserTimeMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

@Service
public class UserTimeMapperImpl implements UserTimeApi {

    @Autowired
    private UserTimeMapper userTimeMapper;

    @Override
    public Integer selectActiveUser(String startTime, String endTime) {

        Integer count = userTimeMapper.selectCount(new QueryWrapper<UserTime>().select("DISTINCT uid").between("login_time", startTime, endTime));
        return count;
    }

    @Override
    public Integer selectActiveUser(String today) {

        Integer count = userTimeMapper.selectCount(new QueryWrapper<UserTime>().select("DISTINCT uid").like("login_time", today));
        return count;
    }

    @Override
    public void saveLoginTime(Long id, Date loginTime) {
        userTimeMapper.insert(UserTime.builder().uid(id).loginTime(loginTime).build());
    }

    @Override
    public Integer selectLoginToday(String pastDate) {
        return userTimeMapper.selectCount(Wrappers.lambdaQuery(UserTime.class).like(UserTime::getLoginTime, pastDate));
    }

    @Override
    public Integer selectNewUserCount(String startDate, String endDate) {
        return userTimeMapper.selectCount(Wrappers.lambdaQuery(UserTime.class).between(UserTime::getLoginTime, startDate, endDate));
    }

    @Override
    public Integer selectUserLogin(List<Long> values, String oneDay1, String oneDay2) {
        Integer count = userTimeMapper.selectCount(Wrappers.lambdaQuery(UserTime.class).in(UserTime::getUid, values).between(UserTime::getLoginTime, oneDay1, oneDay2));
        return count;
    }
}
