package com.itheima.interfaces;

import com.itheima.commons.pojo.Settings;

public interface SettingsApi {
    /**
     * 根据id 查询设置
     * @param userId
     * @return
     */
    Settings selectById(Long userId);

    /**
     * 修改通知设置
     * @param userId
     * @param i
     * @param i1
     * @param i2
     */
    void updateNotifications(Long userId, Long i, Long i1, Long i2);
}
