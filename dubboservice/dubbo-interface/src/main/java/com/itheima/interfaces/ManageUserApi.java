package com.itheima.interfaces;

import com.itheima.commons.pojo.ManageUser;

public interface ManageUserApi {

    ManageUser login(String username, String password);
}
