package com.itheima.mysql.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.commons.pojo.BlackList;

public interface BlacklistMapper extends BaseMapper<BlackList> {
}
