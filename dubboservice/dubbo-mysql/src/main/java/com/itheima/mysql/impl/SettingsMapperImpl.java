package com.itheima.mysql.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.itheima.commons.pojo.Settings;
import com.itheima.interfaces.SettingsApi;
import com.itheima.mysql.mapper.SettingsMapper;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class SettingsMapperImpl implements SettingsApi {
    @Autowired
    private SettingsMapper settingsMapper;

    @Override
    public Settings selectById(Long userId) {
        return settingsMapper.selectOne(Wrappers.lambdaQuery(Settings.class).eq(Settings::getUserId, userId));
    }

    @Override
    public void updateNotifications(Long userId, Long i, Long i1, Long i2) {
        settingsMapper.update(Settings.builder().likeNotification(i).pinglunNotification(i1).gonggaoNotification(i2).build(), Wrappers.lambdaQuery(Settings.class).eq(Settings::getUserId, userId));
    }
}
