package com.itheima.interfaces;

import com.itheima.commons.vo.OperationLog;

import java.util.List;

public interface OperationLogApi {
    List<OperationLog> selectOperation(String st, String et, String name, String text);
}
