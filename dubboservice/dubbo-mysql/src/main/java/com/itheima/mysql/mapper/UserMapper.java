package com.itheima.mysql.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.commons.pojo.User;

public interface UserMapper extends BaseMapper<User> {

}
