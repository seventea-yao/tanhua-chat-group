package com.itheima.commons.pojo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
//选项
public class Options implements Serializable {
    private Long id;
    private Long questionId;
    //@TableField("`option`")
    private String options;

}
