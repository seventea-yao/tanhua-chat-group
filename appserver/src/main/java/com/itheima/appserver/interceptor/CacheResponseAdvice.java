/*
package com.itheima.appserver.interceptor;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONUtil;
import com.itheima.appserver.annotation.MyCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.MediaType;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.util.concurrent.TimeUnit;

@ControllerAdvice
public class CacheResponseAdvice implements ResponseBodyAdvice {

    @Autowired
    RedisTemplate<String,String> redisTemplate;
    */
/**
     * 为true 会进入下面的方法
     * @param methodParameter
     * @param aClass
     * @return
     *//*

    @Override
    public boolean supports(MethodParameter methodParameter, Class aClass) {
        return methodParameter.hasMethodAnnotation(GetMapping.class) && methodParameter.hasMethodAnnotation(MyCache.class);
    }

    */
/**
     * 响应回去前的增强方法
     *
     * @param o                  响应体
     * @param methodParameter
     * @param mediaType
     * @param aClass
     * @param serverHttpRequest
     * @param serverHttpResponse
     * @return
     *//*

    @Override
    public Object beforeBodyWrite(Object o,
                                  MethodParameter methodParameter,
                                  MediaType mediaType,
                                  Class aClass,
                                  ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse) {
        if (ObjectUtil.isEmpty(o)) {
            return null;
        }
        //o 响应回去是json类型 先转换成string类型
        String body = null;
        if (o instanceof String){
            body = (String) o;
        }else {
            body = JSONUtil.toJsonStr(o);
        }

        // 获得唯一key值
        String redisKey = CacheInterceptor.redisKey(((ServletServerHttpRequest)serverHttpRequest).getServletRequest());

        //设置key 的时间 数据
        MyCache methodAnnotation = methodParameter.getMethodAnnotation(MyCache.class);

        redisTemplate.opsForValue().set(redisKey,body,methodAnnotation.time(), TimeUnit.SECONDS);

        return body;
    }
}
*/
