package com.itheima.interfaces;

import com.itheima.commons.pojo.Comment;
import com.itheima.commons.pojo.Publish;
import org.bson.types.ObjectId;

import java.util.List;

public interface PublishApi {
    /**
     * 添加发布信息 返回发布表id
     *
     * @param publish
     * @return
     */
    ObjectId publishMessage(Publish publish);

    /**
     * 查询自己的时间线表
     * 拿到表中所有发布id
     * 去发布表查询所有发布id返回
     */

    List<Publish> selectPublishId(Integer page, Integer pagesize, Long userId);

    /**
     * 通过pid 查询发布表信息
     * @param pid
     * @return
     */
    List<Publish> selectPublishByPid(List<Long> pid);

    List<Publish> randomUserPid(Integer pagesize);

    /**
     * 朋友圈点赞
     * @param comment
     * @return
     */
    Long likeComment(Comment comment);

    /**
     * 朋友圈取消点赞
     * @param publishId
     * @param userId
     * @return
     */
    Long NoLikeComment(ObjectId publishId,Long userId);

    /**
     * 朋友圈喜欢
     * @param comment
     * @return
     */
    Long loveComment(Comment comment);

    /**
     * 朋友圈取消喜欢
     * @param publishId
     * @param userId
     * @return
     */
    Long NoLoveComment(ObjectId publishId,Long userId);


    Publish queryById(ObjectId id);

    /**
     * 根据评论表的publishId 查询发布表的id
     * @param publishId
     * @return
     */
    Publish selectByPublishId(String publishId);

    /**
     * 通过相册表中的id 查询发布表中的publishId
     * @param publish
     * @return
     */
    List<Publish> selectAlbumById(List<String> publish);



    /**
     * 根据动态Id 查询 1点赞 2评论 3喜欢 的总数量
     * @param publishId
     * @param commentType
     * @return
     */
    Long findCount(ObjectId publishId,Integer commentType);

    /**
     * 根据Publish 的Id集合 取List<Publish>
     * @param publishIds
     * @return
     */
    List<Publish> queryPublishes(List<ObjectId> publishIds);
}
