package com.itheima.commons.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collections;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserProportionVo {   //用户数据占比

    private List<?> thisYear = Collections.emptyList(); //今年数据
    private List<?> lastYear = Collections.emptyList(); //去年数据

}
