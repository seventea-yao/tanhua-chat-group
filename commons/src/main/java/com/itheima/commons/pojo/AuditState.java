package com.itheima.commons.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
//消息审核状态表
public class AuditState implements Serializable {

    private Long id;//唯一键
    private Long userId;//发布人用户id
    private Integer topState;//置顶状态，1为未置顶，2为置顶
    private String state;//消息状态
    private String publishId;//消息Id
    private Date created;//创建时间
    private Date updated;//更新时间
}
