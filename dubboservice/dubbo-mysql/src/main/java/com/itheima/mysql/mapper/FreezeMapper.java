package com.itheima.mysql.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.commons.pojo.Freeze;

public interface FreezeMapper extends BaseMapper<Freeze> {
}
