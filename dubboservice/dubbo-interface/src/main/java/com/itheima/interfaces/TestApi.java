package com.itheima.interfaces;
import com.itheima.commons.pojo.*;
import com.itheima.commons.vo.OptionVo;

import java.util.List;
import java.util.Map;

public interface TestApi {


    List<Questionnaire> findQuestionnaire();


    List<QuestionsTest> findQusetions();

    Map<Long, List<OptionVo>> findOption(List<Long> ids);


    Long findScore(String optionId, String questionId);


    void saveReport(Long userId, int reportId);


    Conclusion findConclusion(String id);


    List<Like> findUser(String id);

    Lock findLock(Long userId);


    void updateLock(Lock lock);

    void saveLock(Lock lock1);
}
