package com.itheima.commons.vo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class QuestionsVo {

    private String id;

    private String question;

    private List<OptionVo> options;
}
