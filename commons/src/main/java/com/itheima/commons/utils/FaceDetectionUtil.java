package com.itheima.commons.utils;

import cn.hutool.core.codec.Base64;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.baidu.aip.face.AipFace;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

public class FaceDetectionUtil {
    private FaceDetectionUtil() {
    }
    //设置APPID/AK/SK
    public static  String APP_ID ;
    public static  String API_KEY ;
    public static  String SECRET_KEY ;


    static {
        Properties properties = new Properties();
        try {
            properties.load(SendSMSUtil.class.getClassLoader().getResourceAsStream("baiduyun.properties"));
            APP_ID =properties.getProperty("face.app_id");
            API_KEY =properties.getProperty("face.app_key");
            SECRET_KEY =properties.getProperty("face.secret_key");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Boolean FaceDetection(byte[] imagesFace){
        // 初始化一个AipFace
        AipFace client = new AipFace(APP_ID, API_KEY, SECRET_KEY);

        // 可选：设置网络连接参数
        client.setConnectionTimeoutInMillis(2000);
        client.setSocketTimeoutInMillis(60000);
        // 调用接口
        // 人脸检测
        JSONObject res = client.detect(Base64.encode(imagesFace), "BASE64",null);
        System.out.println(res.toString(2));
        return res.getString("error_msg").equalsIgnoreCase("SUCCESS");
    }
}
