package com.itheima.commons.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Question implements Serializable {

    private Long id;
    private Long userId;
    //问题内容
    private String txt;
    private Date created;
    private Date updated;
}