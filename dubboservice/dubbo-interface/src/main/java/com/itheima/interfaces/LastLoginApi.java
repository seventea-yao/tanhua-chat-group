package com.itheima.interfaces;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.commons.pojo.LastLogin;

import java.util.List;

public interface LastLoginApi {

    /**
     * 添加记录
     * @param lastLogin
     */
    void addLastLogin(LastLogin lastLogin);

    /**
     * 根据Id查询
     * @param userId
     * @return
     */
    LastLogin findLastLoginByUserId(Long userId);

    /**
     * 分页查询
     * @param page
     * @param pagesize
     * @return
     */
    Page<LastLogin> queryLastLogins(Integer page, Integer pagesize);

    /**
     * 查询LastLogin
     * @param userIds 被查询Id集合
     * @return
     */
    List<LastLogin> queryLastLoginList(List<Long> userIds);

}
