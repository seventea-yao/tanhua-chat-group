package com.itheima.commons.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
//动态记录Vo 同时他还是动态管理的一部分
public class MessagesVo implements Serializable {
    private String id;//编号
    private String nickname;//作者昵称
    private Long userId;//作者ID
    private String userLogo;//作者头像
    private Long createDate;//发布日期
    private String text;//正文
    private String state;//审核状态，1为待审核，2为自动审核通过，3为待人工审核，4为人工审核拒绝，5为人工审核通过，6为自动审核拒绝
    private Long reportCount;//举报数
    private Long likeCount;//点赞数
    private Long commentCount;//评论数
    private Long forwardingCount;//转发数
    private String[] medias;//图片列表
    private Integer topState;//置顶状态，1为未置顶，2为置顶
}
