package com.itheima.commons.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Settings implements Serializable {

    private Long id;
    private Long userId;

    private Long likeNotification;
    private Long pinglunNotification;
    private Long gonggaoNotification;

    private Date created;
    private Date updated;
}
