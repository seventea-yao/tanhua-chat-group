package com.itheima.appserver.controller;

import com.itheima.appserver.annotation.NoAuthorization;
import com.itheima.appserver.service.UserInfoService;
import com.itheima.appserver.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("user")
public class UserController {


    @Autowired
    private UserService userService;

    @Autowired
    private UserInfoService userInfoService;

    /**
     * 用户登录
     * @param map 用户手机号
     * @return
     */
    @PostMapping("login")
    @NoAuthorization
    public ResponseEntity userLogin(@RequestBody Map<String,String> map){
        userService.userSendLoginMsg(map.get("phone"));
        return ResponseEntity.ok().body(null);
    }

    /**
     * 用户登录校验
     * @param map 用户手机号和验证码
     * @return
     */
    @PostMapping("loginVerification")
    @NoAuthorization
    public ResponseEntity userLoginVerification(@RequestBody Map<String,String> map){
        HashMap<String,Object> hashMap=userService.userLoginVerification(map);
        return ResponseEntity.ok().body(hashMap);
    }


    /**
     * 用户信息
     * @param token 密文
     * @param map 用户参数
     * @return
     */
    @PostMapping("loginReginfo")
    public ResponseEntity userLoginReginfo(@RequestBody Map<String,String>map){
        userInfoService.loginReginfo(map);
        return ResponseEntity.ok().body(null);
    }

    /**
     * 上传/修改头像
     * @param token 密文
     * @param multipartFile 图片
     * @return
     */
    @PostMapping("loginReginfo/head")
    public ResponseEntity updateHead(@RequestParam("headPhoto") MultipartFile multipartFile){
        userInfoService.updateHead(multipartFile);
        return ResponseEntity.ok().body(null);
    }

}
