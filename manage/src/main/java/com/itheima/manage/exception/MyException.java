package com.itheima.manage.exception;


import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class MyException extends RuntimeException {

    //自定义异常

    private ResultError resultError;

    public MyException(ResultError resultError) {
        super(resultError.getErrMessage());
        this.resultError=resultError;
    }


}
