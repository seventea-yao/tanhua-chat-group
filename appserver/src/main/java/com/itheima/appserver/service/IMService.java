package com.itheima.appserver.service;

import com.itheima.commons.vo.PageResult;
import com.itheima.commons.vo.UserInfoVo;

public interface IMService {
    UserInfoVo selectUserInfo(String huanxinId);

    void contacts(Integer userId);

    PageResult selectLinkman(Integer page, Integer pagesize, String keyword);

    PageResult selectListCount(Integer page, Integer pagesize, Long userId, int i);

    PageResult selectAnnouncements(Integer page, Integer pagesize);
}
