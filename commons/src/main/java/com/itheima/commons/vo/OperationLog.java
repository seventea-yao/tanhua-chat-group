package com.itheima.commons.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OperationLog implements Serializable{
    private Integer id;
    private String name;
    private String ip;
    private String operationTime;
    private String operationText;
    private String description;

}
