package com.itheima.mysql.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.itheima.commons.pojo.Freeze;
import com.itheima.interfaces.FreezeApi;
import com.itheima.mysql.mapper.FreezeMapper;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class FreezeApiIpml implements FreezeApi {
    @Autowired
    private FreezeMapper freezeMapper;

    @Override
    public Boolean addFreeze(Freeze freeze) {
        return freezeMapper.insert(freeze) == 1;
    }

    @Override
    public Boolean deleteFreezeByUserId(Integer userId) {
        UpdateWrapper<Freeze> wrapper = new UpdateWrapper<>();
        wrapper.eq("user_id", userId);
        wrapper.set("state", 0);
        return freezeMapper.update(null, wrapper) == 1;
    }

    @Override
    public Freeze quweyFreezeByUserId(Long userIdL) {
        return freezeMapper.selectOne(Wrappers.lambdaQuery(Freeze.class).eq(Freeze::getUserId, userIdL).ne(Freeze::getState, 0));
    }

}
