package com.itheima.mysql.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.commons.pojo.UserInfo;

public interface UserInfoMapper extends BaseMapper<UserInfo> {

}
