package com.itheima.commons.pojo;


import com.google.j2objc.annotations.ObjectiveCName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Distribution {
    private String title;
    private Integer amount;
}
