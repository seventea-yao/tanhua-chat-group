package com.itheima.commons.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Answers implements Serializable {
    //试题编号
    private String questionId;
    //选项编号
    private String optionId;
}
