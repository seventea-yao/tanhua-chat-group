package com.itheima.appserver.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;

public interface UserInfoService {

    void loginReginfo(Map<String, String> map);

    void updateHead(MultipartFile multipartFile);
}
