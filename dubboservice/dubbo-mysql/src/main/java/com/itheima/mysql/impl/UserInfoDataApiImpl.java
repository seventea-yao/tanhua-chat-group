package com.itheima.mysql.impl;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.itheima.commons.pojo.UserInfo;
import com.itheima.commons.pojo.UserInfoData;
import com.itheima.interfaces.UserInfoDataApi;
import com.itheima.mysql.mapper.UserInfoDataMapper;
import com.itheima.mysql.mapper.UserInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service
public class UserInfoDataApiImpl implements UserInfoDataApi {
    @Autowired
    private UserInfoDataMapper userInfoDataMapper;

    @Override
    public List<UserInfoData> selectNowDayUser(String dateTime, String endOfDay, Integer sex) {
        List<UserInfoData> selectList = userInfoDataMapper.selectList(Wrappers.lambdaQuery(UserInfoData.class)
                .eq(ObjectUtil.isNotNull(sex), UserInfoData::getSex, sex)
                .between(UserInfoData::getCreated, dateTime, endOfDay));
        return selectList;
    }
}
