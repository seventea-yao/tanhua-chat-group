package com.itheima.appserver.service;

public interface TokenService {

    String getToken();

    String refreshToken();
}
