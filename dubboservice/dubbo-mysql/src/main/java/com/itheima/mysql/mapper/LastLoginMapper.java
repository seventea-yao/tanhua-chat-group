package com.itheima.mysql.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.commons.pojo.LastLogin;

public interface LastLoginMapper extends BaseMapper<LastLogin> {
}
