package com.itheima.appserver.interceptor;
import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.commons.constants.Constants;
import com.itheima.commons.pojo.User;
import com.itheima.interfaces.UserApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import java.util.List;

/**
 * @author byZhao
 * @motto Talk is cheap . Show me the code!
 * @modify 2021-11-17 15:25
 * @description 缓存预热功能！ 【严重功能缺陷：缓存不同步】
 * <p>
 * spring task 定时任务：【基础班 ：Timer  定时任务调度】
 */
@Component
public class RedisPreCache implements ApplicationRunner {
    @Autowired
    RedisTemplate redisTemplate;

    @Reference
    UserApi userApi;


    @Override
    public void run(ApplicationArguments args) throws Exception {

        preCache();
    }
    @Scheduled(cron = "0 0 0 * * ?")
    public void preCache() {
        List<User> user= userApi.selectAll();
        for (User user1 : user) {
            redisTemplate.opsForValue().set(Constants.SOUND + user1.getId(),10);
        }
    }
}
