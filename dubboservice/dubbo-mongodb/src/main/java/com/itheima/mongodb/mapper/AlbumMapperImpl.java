package com.itheima.mongodb.mapper;

import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.commons.pojo.Album;
import com.itheima.interfaces.AlbumApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

@Service
public class AlbumMapperImpl implements AlbumApi {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public void savePublishId(Album album, Long userId) {
        mongoTemplate.save(album, "quanzi_album_" + userId);
    }

    @Override
    public List<Album> selectByUserId(Integer page, Integer pagesize, Long userId) {
        List<Album> albumList = mongoTemplate.find(new Query().with(PageRequest.of(page - 1, pagesize)), Album.class, "quanzi_album_" + userId);
        return albumList;
    }

    @Override
    public List<Album> selectByUserIdAndDesc(Integer page, Integer pagesize, Long userId) {
        return mongoTemplate.find(new Query().with(PageRequest.of(page - 1, pagesize))
                        .with(Sort.by(Sort.Order.desc("created"))), Album.class, "quanzi_album_" + userId);
    }

    @Override
    public Long findCountByUserId(Long userId) {
        return mongoTemplate.count(new Query(),Album.class,"quanzi_album_" + userId);
    }
}
