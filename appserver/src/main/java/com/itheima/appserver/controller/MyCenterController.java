package com.itheima.appserver.controller;

import cn.hutool.core.convert.Convert;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.itheima.appserver.service.MyCenterService;
import com.itheima.commons.vo.CountsVo;
import com.itheima.commons.vo.PageResult;
import com.itheima.commons.vo.SettingsVo;
import com.itheima.commons.vo.UserInfoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

@RestController
@RequestMapping("users")
public class MyCenterController {

    @Autowired
    private MyCenterService myCenterService;

    /**
     * 根据用户id 查询用户资料
     *
     * @param userID
     * @return
     */
    @GetMapping
    public ResponseEntity MyCenter(@RequestParam(value = "userID", required = false) Long userID) {
        UserInfoVo userInfoVo = myCenterService.queryUserInfoByUserId(userID);
        return ResponseEntity.ok(userInfoVo);
    }

    /**
     * 喜欢 粉丝 互相喜欢统计
     *
     * @return
     */
    @GetMapping("counts")
    public ResponseEntity counts() {
        CountsVo countsVo = myCenterService.counts();
        return ResponseEntity.ok(countsVo);
    }

    /**
     * 互相喜欢、喜欢、粉丝、谁看过我 - 翻页列表
     * 1 互相关注 2 我关注 3 粉丝 4 谁看过我
     *
     * @param type
     * @param page
     * @param pagesize
     * @param nickname
     * @return
     */
    @GetMapping("friends/{type}")
    public ResponseEntity queryLikeList(@PathVariable("type") Integer type,
                                        @RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                        @RequestParam(value = "pagesize", required = false, defaultValue = "10") Integer pagesize,
                                        @RequestParam(value = "nickname", required = false) String nickname) {
        PageResult pageResult = myCenterService.queryLikeList(type, page, pagesize, nickname);
        return ResponseEntity.ok(pageResult);
    }

    /**
     * 取消喜欢
     *
     * @param uid
     * @return
     */
    @DeleteMapping("like/{uid}")
    public ResponseEntity cancelLove(@PathVariable("uid") Long uid) {
        myCenterService.cancelLove(uid);
        return ResponseEntity.ok(null);
    }

    /**
     * 喜欢粉丝
     *
     * @param uid
     * @return
     */
    @PostMapping("fans/{uid}")
    public ResponseEntity fansLove(@PathVariable("uid") Long uid) {
        myCenterService.fansLove(uid);
        return ResponseEntity.ok(null);
    }

    /**
     * 修改用户信息
     *
     * @return
     */
    @PutMapping()
    public ResponseEntity putUserInfo(@RequestBody Map<String, Object> map) {
        String nickname = Convert.toStr(map.get("nickname"));
        String birthday = Convert.toStr(map.get("birthday"));
        myCenterService.putUserInfo(nickname, birthday);
        return ResponseEntity.ok(null);
    }

    /**
     * 修改用户头像
     *
     * @param headPhoto
     * @return
     */
    @PostMapping("header")
    public ResponseEntity updateHeader(@RequestParam("headPhoto") MultipartFile headPhoto) {
        myCenterService.updateHeader(headPhoto);
        return ResponseEntity.ok(null);
    }


    /**
     * 查询用户通知设置
     *
     * @return
     */
    @GetMapping("settings")
    public ResponseEntity getSettings() {
        SettingsVo settingsVo = myCenterService.getSettings();
        return ResponseEntity.ok(settingsVo);
    }

    /**
     * 设置陌生人问题
     *
     * @param content
     * @return
     */
    @PostMapping("questions")
    public ResponseEntity settingQuestion(@RequestBody Map<String, String> content) {
        myCenterService.settingQuestion(content.get("content"));
        return ResponseEntity.ok(null);
    }

    /**
     * 黑名单查询
     * @param page
     * @param pagesize
     * @return
     */
    @GetMapping("blacklist")
    public ResponseEntity blacklist(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                    @RequestParam(value = "pagesize", required = false, defaultValue = "10") Integer pagesize) {
         PageResult pageResult =   myCenterService.blacklist(page,pagesize);
        return ResponseEntity.ok(pageResult);
    }

    /**
     * 移除黑名单
     *
     * @param uid
     * @return
     */
    @DeleteMapping("blacklist/{uid}")
    public ResponseEntity removeBlackList(@PathVariable("uid") Long uid) {
        myCenterService.removeBlackList(uid);
        return ResponseEntity.ok(null);
    }

    /**
     * 修改 通知设置
     * @param map
     * @return
     */
    @PostMapping("notifications/setting")
    public ResponseEntity notifications(@RequestBody Map<String, Boolean> map) {
        Boolean likeNotification = map.get("likeNotification");
        Boolean pinglunNotification = map.get("pinglunNotification");
        Boolean gonggaoNotification = map.get("gonggaoNotification");
        myCenterService.notifications(likeNotification,pinglunNotification,gonggaoNotification);
        return ResponseEntity.ok(null);
    }

    /**
     * 修改手机号- 1 发送短信验证码
     * @return
     */
    @PostMapping("phone/sendVerificationCode")
    public ResponseEntity sendVerificationCode(){
        myCenterService.sendVerificationCode();
        return ResponseEntity.ok(null);
    }

    /**
     * 校验验证码
     * @param map
     * @return
     */
    @PostMapping("phone/checkVerificationCode")
    public ResponseEntity checkVerificationCode(@RequestBody Map<String,String>map){
        Boolean verification = myCenterService.checkVerificationCode(map.get("verificationCode"));
        JSONObject jsonObject = JSONUtil.createObj().set("verification", verification);
        return ResponseEntity.ok(jsonObject);
    }

    /**
     * 修改手机号
     * @param
     * @return
     */
    @PostMapping("phone")
    public ResponseEntity savePhone(@RequestBody Map<String,String>map){
        myCenterService.savePhone(map.get("phone"));
        return ResponseEntity.ok(null);
    }

}
