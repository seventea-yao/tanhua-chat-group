package com.itheima.interfaces;

import com.itheima.commons.pojo.RecommendUser;
import com.itheima.commons.pojo.UserLike;
import com.itheima.commons.pojo.Visitors;
import com.itheima.commons.vo.PageResult;

import java.util.List;

public interface TanHuaApi {

    RecommendUser todayBestByToUserId(Long toUserId);


    PageResult recommendationAllFriend(Integer page, Integer pagesize, Long toUserId);

    /**
     * 根据用户id 和自己的id 查询心动值
     * @param id
     * @param userId
     * @return
     */
    RecommendUser findScoreByRecommend(Long id, Long userId);

    /**
     * 随机生成userId
     * @param userId
     * @return
     */
    List<RecommendUser> selectByToUserId(Long userId);

    /**
     * 添加喜欢
     * @param userLike 喜欢用户id
     */
    void loveCard(UserLike userLike);

    /**
     * 保存访客记录
     * @param visitors
     */
    void savePersonInfo(Visitors visitors);
}
