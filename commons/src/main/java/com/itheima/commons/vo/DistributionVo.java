package com.itheima.commons.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DistributionVo { //分布视图
    private List<?> industryDistribution = Collections.emptyList();
    private List<?> ageDistribution = Collections.emptyList();
    private List<?> genderDistribution = Collections.emptyList();
    private List<?> localDistribution = Collections.emptyList();
    private List<?> localTotal = Collections.emptyList();

}
