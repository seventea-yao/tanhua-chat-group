package com.itheima.mongodb.mapper;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.commons.pojo.UserLike;
import com.itheima.commons.pojo.Users;
import com.itheima.commons.pojo.Visitors;
import com.itheima.interfaces.UsersApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

@Service
public class UsersMapperImpl implements UsersApi {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public void saveUsers(Users users) {
        mongoTemplate.save(users);
    }

    @Override
    public List<Users> selectFriendByUserId(Integer page, Integer pagesize, String keyword, Long userId) {
        if (StrUtil.isEmpty(keyword)) {
            return mongoTemplate.find(Query.query(Criteria.where("userId").is(userId)).with(PageRequest.of(page - 1, pagesize)), Users.class);
        }

        return mongoTemplate.find(Query.query(Criteria.where("userId").is(userId)), Users.class);
    }

    @Override
    public Long selectLove(Long userId) {
        return mongoTemplate.count(Query.query(Criteria.where("userId").is(userId)), UserLike.class);
    }

    @Override
    public Long selectFans(Long userId) {
        return mongoTemplate.count(Query.query(Criteria.where("likeUserId").is(userId)), UserLike.class);
    }

    @Override
    public List<Long> selectLoveId(Long userId) {
        List<UserLike> likeList = mongoTemplate.find(Query.query(Criteria.where("userId").is(userId)), UserLike.class);
        List<Long> likeUserId = CollUtil.getFieldValues(likeList, "likeUserId", Long.class);
        return likeUserId;
    }

    @Override
    public Long selectEachLove(List<Long> idList, Long id) {
        return mongoTemplate.count(Query.query(Criteria.where("userId").in(idList).and("likeUserId").is(id)), UserLike.class);
    }

    @Override
    public List<Long> selectLovePage(Long userId, Integer page, Integer pagesize) {
        List<UserLike> likeList = mongoTemplate.find(Query.query(Criteria.where("userId").is(userId))
                .with(PageRequest.of(page - 1, pagesize)), UserLike.class);
        return CollUtil.getFieldValues(likeList, "likeUserId", Long.class);

    }

    @Override
    public List<Long> selectFansPage(Long userId, Integer page, Integer pagesize) {
        List<UserLike> userLikes = mongoTemplate.find(Query.query(Criteria.where("likeUserId").is(userId))
                .with(PageRequest.of(page - 1, pagesize)), UserLike.class);

        return CollUtil.getFieldValues(userLikes, "userId", Long.class);
    }

    @Override
    public List<Long> selectEachLovePage(List<Long> selectLoveId, Long userId, Integer page, Integer pagesize) {
        List<UserLike> userLikes = mongoTemplate.find(Query.query(Criteria.where("userId").in(selectLoveId).and("likeUserId").is(userId))
                        .with(PageRequest.of(page - 1, pagesize))
                , UserLike.class);

        return CollUtil.getFieldValues(userLikes, "userId", Long.class);
    }

    @Override
    public List<Long> visitors(Long userId, Integer page, Integer pagesize) {
        List<Visitors> visitorsList = mongoTemplate.find(Query.query(Criteria.where("userId").is(userId)), Visitors.class);

        return CollUtil.getFieldValues(visitorsList, "visitorUserId", Long.class);
    }

    @Override
    public UserLike aleadyLove(Long userId, Long userId1) {
        UserLike userLike = mongoTemplate.findOne(Query.query(Criteria.where("userId").is(userId).and("likeUserId").is(userId1)), UserLike.class);
        return userLike;
    }

    @Override
    public List<Visitors> visitorsMyHome(Long userId, Long userTime) {
        //判断是否已经看过
        if (ObjectUtil.isNotEmpty(userTime)) {
            //已经看过
            return mongoTemplate.find(Query.query(Criteria.where("userId").is(userId).and("date").gt(userTime))
                            .with(PageRequest.of(0, 5))
                    , Visitors.class);
        }
        //没有看过 给他前5条
        return mongoTemplate.find(Query.query(Criteria.where("userId").is(userId)).with(PageRequest.of(0, 5)), Visitors.class);

    }

    @Override
    public void deleteUserId(Long id, Long uid) {
        mongoTemplate.remove(Query.query(Criteria.where("userId").is(id).and("likeUserId").is(uid)), UserLike.class);
    }

    @Override
    public UserLike selectIsEachLove(Long userId, Long uid) {
        return mongoTemplate.findOne(Query.query(Criteria.where("userId").is(uid).and("likeUserId").is(uid)), UserLike.class);
    }

    @Override
    public void saveUserLike(UserLike userLike) {
        mongoTemplate.save(userLike);
    }

}
