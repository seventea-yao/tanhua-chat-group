package com.itheima.commons.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostLoginDayVo implements Serializable {
    private String date;
    private Integer register;
    private String oneDay;
    private String twoDay;
    private String threeDay;
    private String fourDay;
    private String fiveDay;
    private String sixDay;
    private String sevenDay;
    private String thirtyDay;

}