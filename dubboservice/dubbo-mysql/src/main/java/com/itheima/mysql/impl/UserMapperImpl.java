package com.itheima.mysql.impl;

import cn.hutool.core.date.DateTime;
import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.itheima.commons.pojo.User;
import com.itheima.interfaces.UserApi;
import com.itheima.mysql.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

@Service
public class UserMapperImpl implements UserApi {

    @Autowired
    private UserMapper userMapper;

    @Override
    public User selectUserByPhone(String phone) {
        return userMapper.selectOne(Wrappers.lambdaQuery(User.class).eq(User::getMobile, phone));
    }

    @Override
    public User addUser(User user) {
        userMapper.insert(user);
        return user;
    }

    @Override
    public User selectPhoneById(Long userId) {
        return userMapper.selectOne(Wrappers.lambdaQuery(User.class).eq(User::getId, userId));
    }

    @Override
    public void savePhone(Long userId, String phone) {
        userMapper.update(User.builder().mobile(phone).build(), Wrappers.lambdaQuery(User.class).eq(User::getId, userId));
    }

    @Override
    public Integer selectUserCount() {
        return userMapper.selectCount(Wrappers.lambdaQuery(User.class));
    }

    @Override
    public List<User> selectAll() {
        List<User> users = userMapper.selectList(Wrappers.lambdaQuery(User.class));
        return users;
    }

    @Override
    public Integer selectNewUser(String pastDate) {
        return userMapper.selectCount(Wrappers.lambdaQuery(User.class).like(User::getCreated, pastDate));
    }

    @Override
    public Integer selectNewUserCount(String startDate, String endDate) {
        return userMapper.selectCount(Wrappers.lambdaQuery(User.class).between(User::getCreated, startDate, endDate));
    }

    @Override
    public int allCount() {
        return userMapper.selectList(null).size();
    }

    @Override
    public int registerUser(Date start,  Date end) {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        return userMapper.selectCount(queryWrapper.between("created", start, end)
        );
    }

    @Override
    public List<User> registerUserList(Date start, Date end) {
        return userMapper.selectList(Wrappers.lambdaQuery(User.class).between(User::getCreated, start, end).orderByAsc(User::getCreated));
    }

    @Override
    public List<User> selectNowDayUser(String dateTime, String endOfDay,String sex) {
        List<User> selectList = userMapper.selectList(Wrappers.lambdaQuery(User.class).between(User::getCreated, dateTime, endOfDay));
        return selectList;
    }
}
