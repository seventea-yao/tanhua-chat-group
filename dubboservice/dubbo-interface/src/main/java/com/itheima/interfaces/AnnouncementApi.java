package com.itheima.interfaces;

import com.itheima.commons.pojo.Announcement;

import java.util.List;

public interface AnnouncementApi {
    List<Announcement> selectAnnouncement();

}
