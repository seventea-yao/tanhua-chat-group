package com.itheima.commons.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
//拼凑vo对象
public class SimilarYou implements Serializable {
    //用户Id
    private int id;
    //用户头像
    private String avatar;
}
