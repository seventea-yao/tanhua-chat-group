package com.itheima.commons.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SettingsVo implements Serializable {
    // 编号
    private Integer id;
    // 陌生人问题
    private String strangerQuestion;
    // 手机号
    private String phone;
    // 推送喜欢通知
    private Boolean likeNotification;
    // 推送评论通知
    private Boolean pinglunNotification;
    //推送公告通知
    private Boolean gonggaoNotification;
}
