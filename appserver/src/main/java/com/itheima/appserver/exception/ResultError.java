package com.itheima.appserver.exception;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ResultError {

    //定义异常的参数

    private String errCode;

    private String errMessage;


    //未知错误
    public static ResultError error(){
        return new ResultError("999999","未知错误,请稍后重试");
    }

    public static ResultError phoneError(){
        return ResultError.builder().errCode("100002").errMessage("手机号码格式不正确").build();
    }
    public static ResultError phoneIsError(){
        return ResultError.builder().errCode("100022").errMessage("手机号码已存在").build();
    }

    public static ResultError sendCodeError(){
        return ResultError.builder().errCode("100003").errMessage("之前的验证码还未过期").build();

    }

    public static ResultError codeError(){
        return ResultError.builder().errCode("100004").errMessage("验证码错误").build();

    }

    public static ResultError sendSMSError() {
        return ResultError.builder().errCode("100005").errMessage("短信发送失败").build();


    }

    public static ResultError tokenLoseError() {
        return ResultError.builder().errCode("100006").errMessage("token是无效的").build();

    }

    public static ResultError tokenOutTimeError() {
        return ResultError.builder().errCode("100007").errMessage("token已过期").build();

    }

    public static ResultError UserInfoError() {
        return ResultError.builder().errCode("100008").errMessage("用户信息有误").build();


    }

    public static ResultError fileFormatError() {
        return ResultError.builder().errCode("100009").errMessage("图片格式有误").build();

    }

    public static ResultError videoFormatError() {
        return ResultError.builder().errCode("100019").errMessage("视频格式有误").build();

    }


    public static ResultError faceError() {
        return ResultError.builder().errCode("100010").errMessage("请上传真人头像").build();


    }

    public static ResultError faceUploadError() {
        return ResultError.builder().errCode("100011").errMessage("头像上传有误").build();


    }

    public static ResultError recommendUserIsEmpty() {
        return ResultError.builder().errCode("200001").errMessage("今日佳人为空").build();

    }


    public static ResultError textContentIsNull() {
        return ResultError.builder().errCode("300001").errMessage("文字不能为空").build();


    }

    public static ResultError videoIsNullError() {
        return ResultError.builder().errCode("400001").errMessage("小视频文件不能为空").build();

    }

    public static ResultError userRegitError() {
        return ResultError.builder().errCode("100101").errMessage("用户注册失败请重试").build();

    }

    public static ResultError sendHuanXinMsgError() {
        return ResultError.builder().errCode("500001").errMessage("环信信息发送失败").build();

    }

    public static ResultError SoundIsNullError() {
      return ResultError.builder().errCode("500000").errMessage("语音发送为空").build();
    }

    public static ResultError SoundIsParseError() {
        return ResultError.builder().errCode("400000").errMessage("语音格式错误").build();
    }

    public static ResultError SoundSizeError() {
        return ResultError.builder().errCode("400002").errMessage("语音大小错误").build();
    }

    public static ResultError TIMESError() {
        return ResultError.builder().errCode("400003").errMessage("今日次数已用完,明天再来").build();
    }
}
