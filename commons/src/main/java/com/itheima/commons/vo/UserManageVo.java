package com.itheima.commons.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
//管理用户展示列表
public class UserManageVo {
    private Integer id;//用户Id
    private String logo;//头像
    private String logoStatus;//头像状态 ,1通过，2拒绝
    private String nickname;//昵称
    private String mobile;//手机号，即用户账号
    private String sex;//性别
    private Integer age;//年龄
    private String occupation;//职业
    private String userStatus;//用户状态
    private Long lastActiveTime;//最近活跃时间
    private String city;//注册城市名称，城市信息叶子节点名称即可，无需adcode

}
