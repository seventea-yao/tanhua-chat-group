package com.itheima.mysql.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.commons.pojo.AuditState;

public interface AuditStateMapper extends BaseMapper<AuditState> {
}
