package com.itheima.appserver.controller;
import com.itheima.appserver.service.impl.TestServiceImpl;
import com.itheima.commons.pojo.Answers;
import com.itheima.commons.vo.QuestionnaireVo;
import com.itheima.commons.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;
@RestController
@RequestMapping("testSoul")
public class TestController {
    @Autowired
    TestServiceImpl testServiceImpl;

    //测灵魂-问卷列表
    @GetMapping
    public ResponseEntity testList() {
        List<QuestionnaireVo> iteam = testServiceImpl.sendSound();
        return ResponseEntity.ok(iteam);
    }

    //测灵魂-查提交问卷
    @PostMapping()
    public ResponseEntity testSubmit(@RequestBody Map<String, List<Answers>> map) {
        List<Answers> answers = map.get("answers");

        String reportId= testServiceImpl.testSubmit(answers);
        return ResponseEntity.ok(reportId);
    }
    //测灵魂-查看结果
    @GetMapping("report/{id}")
    public ResponseEntity testResult(@PathVariable("id") String id) {
        ResultVo resultVo= testServiceImpl.testResult(id);
        return ResponseEntity.ok(resultVo);
    }
}
