package com.itheima.mongodb.mapper;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.commons.pojo.Comment;
import com.itheima.commons.pojo.FollowUser;
import com.itheima.interfaces.CommentApi;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

@Service
public class CommentMapperImpl implements CommentApi {

    @Autowired
    private MongoTemplate mongoTemplate;


    @Override
    public Long selectLikeSize(String id) {
        return selectSize(id, 1);
    }

    @Override
    public Long selectCommentSize(String id) {
        return selectSize(id, 3);
    }

    @Override
    public Long selectLoveSize(String id) {
        return selectSize(id, 2);
    }

    @Override
    public boolean selectIsLike(String id, Long userId) {
        return selectIs(id, userId, 1);
    }

    @Override
    public boolean selectIsLove(String id, Long userId) {
        return selectIs(id, userId, 3);
    }

    @Override
    public void saveComment(Comment comment) {
        mongoTemplate.save(comment);
    }

    @Override
    public Comment selectPublishUserId(ObjectId movementId) {
        Comment publishId = mongoTemplate.findOne(Query.query(Criteria.where("publishId").is(movementId)), Comment.class);
        return publishId;
    }

    @Override
    public Comment selectById(String publishId) {
        Comment comment = mongoTemplate.findOne(Query.query(Criteria.where("id").is(publishId)), Comment.class);
        return comment;
    }

    @Override
    public List<Comment> selectByPublishId(String publishId, Integer page, Integer pagesize) {
        List<Comment> commentList = mongoTemplate.find(Query.query(Criteria.where("publishId").is(new ObjectId(publishId))
                                .and("commentType").is(2))
                        .with(Sort.by(Sort.Order.desc("created")))
                        .with(PageRequest.of(page - 1, pagesize)),
                Comment.class);
        return commentList;
    }

    @Override
    public Boolean selectIsFollow(Long userId, Long userId1) {
        FollowUser followUser = mongoTemplate.findOne(Query.query(Criteria.where("followUserId").is(userId).and("userId").is(userId1)), FollowUser.class);

        return ObjectUtil.isNotEmpty(followUser);
    }

    @Override
    public List<Comment> selectByPublishUserId(Integer page, Integer pagesize, Long userId, int i) {
        return mongoTemplate.find(Query.query(Criteria.where("publishUserId").is(userId).and("commentType").is(i)).with(PageRequest.of(page - 1, pagesize)), Comment.class);
    }

    @Override
    public List<Comment> pageCommentsLogs(Integer page, Integer pageSize, String sortProp, String sortOrder, ObjectId messageID) {
        List<Comment> commentList;
        if ("ascending".equals(sortOrder)) {
            commentList = mongoTemplate.find(Query.query(Criteria.where("publishId").is(messageID).and("commentType").is(2)).with(PageRequest.of(page - 1, pageSize)).with(Sort.by(Sort.Order.asc("created"))), Comment.class);

        } else {
            commentList = mongoTemplate.find(Query.query(Criteria.where("publishId").is(messageID).and("commentType").is(2)).with(PageRequest.of(page - 1, pageSize)).with(Sort.by(Sort.Order.desc("created"))), Comment.class);
        }
        return commentList;
    }


    public boolean selectIs(String id, Long userId, Integer type) {
        List<Comment> commentList = mongoTemplate.find(Query.query(Criteria.where("publishId").is(new ObjectId(id))
                .and("commentType").is(type)
                .and("userId").is(userId)), Comment.class);
        return ObjectUtil.isNotEmpty(commentList);
    }

    public Long selectSize(String id, Integer type) {
        long count = mongoTemplate.count(Query.query(Criteria.where("publishId").is(new ObjectId(id)).and("commentType").is(type)), Comment.class);
        return count;
    }
}
