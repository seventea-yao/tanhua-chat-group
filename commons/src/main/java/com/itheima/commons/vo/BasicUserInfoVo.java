package com.itheima.commons.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
//基本的用户资料
public class BasicUserInfoVo implements Serializable {

    private Long id;//用户Id
    private String nickname;//昵称
    private String mobile;//手机号，即用户账号
    private String sex;//性别
    private String personalSignature;//个性签名
    private Integer age;//年龄
    private Integer countBeLiked;//被喜欢人数
    private Integer countLiked;//喜欢人数
    private Integer countMatching;//配对人数
    private Integer income;//收入
    private String occupation;//职业,暂无该字段
    private String userStatus;//用户状态，1为正常，2为冻结
    private Long created;//注册时间
    private String city;//注册地区
    private Long lastActiveTime;//最近活跃时间
    private String lastLoginLocation;//最近登录地
    private String logo;//用户头像
    private String tags;//用户标签

}
