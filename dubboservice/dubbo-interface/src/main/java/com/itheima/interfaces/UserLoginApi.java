package com.itheima.interfaces;

import cn.hutool.core.date.DateTime;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.commons.pojo.UserLogin;

import java.util.Date;
import java.util.List;

public interface UserLoginApi {

    void addUserLogin(UserLogin userLogin);

    Integer findActiveUser(Date start, Date end);

    Integer loginUserCount(Date start, Date end);

    List<UserLogin> findUser(Date start, Date end);

    Integer selectUserLogin(List<Long> values, DateTime oneDay1, DateTime oneDay2);

    Page<UserLogin> pageLoginLogs(Integer page, Integer pageSize, String sub, Long uid);

    List<UserLogin> findLoginCount(Date startDate, Date endDate,List<Long> ids);
}