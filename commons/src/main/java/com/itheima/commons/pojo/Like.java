package com.itheima.commons.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
//相似表
public class Like implements Serializable {
    //用户id
    private Long id;
    //报告id
    private Long reportId;
}
