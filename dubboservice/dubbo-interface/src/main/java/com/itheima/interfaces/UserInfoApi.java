package com.itheima.interfaces;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.commons.pojo.Question;
import com.itheima.commons.pojo.User;
import com.itheima.commons.pojo.UserInfo;
import com.itheima.commons.vo.LinearVo;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface UserInfoApi {
    /**
     * 添加用户信息
     * @param userInfo
     */
    void insertUserInfo(UserInfo userInfo);

    /**
     * 修改用户头像
     * @param uid
     * @param logo
     */
    void updateUserInfoByLogo(Long uid,String logo);

    /**
     * 根据id 查询用户信息
     * @param uid
     * @return
     */
    UserInfo findUserInfoByUserId(Long uid);

    /**
     * 根据用户id 查询用户信息 封装到map中 键 用户id  值 用户信息 可以根据信息筛选
     * @param userInfo
     * @param userId
     * @return
     */
    Map<Long, UserInfo> findUserInfoListByUserIdsByCondition(UserInfo userInfo, List<Long> userId, Collection<String> strings);

    Map<Long, UserInfo> findUserListByUserIds(UserInfo userInfo, List<Long> userIds);

    Map<Long, UserInfo> findUserInfoListByUserIdsBykeyword( List<Long> userId,String keyword);

    /**
     * 根据用户id 查询用户信息 封装到map中 键 用户id  值 用户信息
     * @param userId
     * @return
     */
    Map<Long, UserInfo> findPublishInfo(List<Long> userId);

    Map<Long, UserInfo> findUserListByUserIdsByGender(UserInfo userInfo, List<Long> userIds,String gender);

    /**
     * 修改用户信息
     * @param userId
     */
    void updateByUserInfo(Long userId,UserInfo userInfo);

    /**
     * 修改用户头像
     * @param id
     * @param originalFilename
     */
    void updateHeader(Long id,String originalFilename);


    /**
     * 查询行业
     */
    List<String> selectIndustry();

    /**
     * 查询行业数量
     * @param industry
     * @param startTime
     * @param endTime
     * @return
     */
    Integer selectIndustryCount(String industry, String startTime, String endTime);

    /**
     * 查询年龄分布情况
     * @param age1
     * @param age2
     * @param startTime
     * @param endTime
     * @return
     */
    Integer selectAgeCount(int age1, int age2, String startTime, String endTime);

    /**
     * 查询性别分布情况
     * @param i
     * @param startTime
     * @param endTime
     * @return
     */
    Integer selectSexCount(int i, String startTime, String endTime);

    /**
     * 查询地区人数
     * @param region
     * @return
     */
    Integer selectRegionCount(String region);


    /**
     * 根据条件对象模糊查询UserInfo
     *  模糊查询条件对象
     * @return
     */
    Page<UserInfo> queryUserInfosByPage(Integer page, Integer pagesize, Long userId, String nickname, String city);

    /**
     * 根据userId集合 获取 UserInfo集合
     * @param userId
     * @return
     */
    List<UserInfo> queryPublishInfos(List<Long> userId);

    List<LinearVo> findListGroupByIndustry(List<Long> ids);

    List<LinearVo> findListGroupByGender(List<Long> ids);

    List<LinearVo> findListGroupByAge(List<Long> ids);

    List<LinearVo> findListGroupByLocation(List<Long> ids);

    List<UserInfo> finduserinfoByuserIdExIID(List<Long> uid, Long userId);

    List<UserInfo> selectNowDayUser(String dateTime, String endOfDay,Integer sex);
}
